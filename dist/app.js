angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'restangular', 'ngFileUpload', 'ngCookies', 'textAngular', 'as.sortable', 'ngImgCrop', 'ngAnimate', 'toastr', 'angular-medium-editor',
'mwl.confirm', 'infinite-scroll', 'sticky', 'satellizer']).

constant( 'baseUrl', 'http://api.gigwise.com').
//constant( 'baseUrl', 'http://api.getblow.co').

config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider, RestangularProvider, $provide, toastrConfig, $tooltipProvider, $authProvider) {
    
    RestangularProvider.setBaseUrl(baseUrl);
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
    //.when('/', { 
    //    controller: 'homeCtrl', 
    //    templateUrl: 'src/app/views/home.html'
    //})
    .when('/latest', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'latest'
    })
    .when('/featured', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'featured'
    })
    .when('/top-users', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'top_users'
    })
    .when('/top-posts', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'top_posts'
    })
     
    .when('/create-feature', { 
        controller: 'create_featureCtrl', 
        templateUrl: 'src/app/views/create_feature.html' 
    })
    .when('/edit-feature/:id', { 
        controller: 'create_featureCtrl', 
        templateUrl: 'src/app/views/create_feature.html' 
    })
    .when('/create-slideshow', { 
        controller: 'create_slideshowCtrl', 
        templateUrl: 'src/app/views/create_slideshow.html' 
    })
    .when('/edit-slideshow/:id', { 
        controller: 'create_slideshowCtrl', 
        templateUrl: 'src/app/views/create_slideshow.html' 
    })
    .when('/create-gigphotos', { 
        controller: 'create_gigphotosCtrl', 
        templateUrl: 'src/app/views/create_gigphotos.html' 
    })
    .when('/edit-gigphotos/:id', { 
        controller: 'create_gigphotosCtrl', 
        templateUrl: 'src/app/views/create_gigphotos.html' 
    })
    .when('/create-review', { 
        controller: 'create_reviewCtrl', 
        templateUrl: 'src/app/views/create_review.html' 
    })
    .when('/edit-review/:id', { 
        controller: 'create_reviewCtrl', 
        templateUrl: 'src/app/views/create_review.html' 
    })
    .when('/create-musicnews', { 
        controller: 'create_musicnewsCtrl', 
        templateUrl: 'src/app/views/create_musicnews.html' 
    })
    .when('/edit-musicnews/:id', { 
        controller: 'create_musicnewsCtrl', 
        templateUrl: 'src/app/views/create_musicnews.html' 
    })
    .when('/profile', { 
        controller: 'profileCtrl', 
        templateUrl: 'src/app/views/profile.html' 
    })
    .when('/settings', { 
        controller: 'profile_settingsCtrl', 
        templateUrl: 'src/app/views/profile_settings.html',
        activetab: 'settings'
    })
    .when('/my-articles', { 
        controller: 'profile_settingsCtrl', 
        templateUrl: 'src/app/views/profile_settings.html',
        activetab: 'my-articles'
    })
    .when('/article/:id/:title', { 
        controller: 'view_articleCtrl', 
        templateUrl: 'src/app/views/view_article.html' 
    })
    .otherwise({ redirectTo: '/latest' });
    
    //textAngular create custom button
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
        // $delegate is the taOptions we are decorating
        // register the tool with textAngular
        taRegisterTool('colourRed', {
            iconclass: "fa fa-image",
            action: function(){
                this.$editor().wrapSelection('forecolor', 'red');
            }
        });
        // add the button to the default toolbar definition
        //taOptions.toolbar[1].push('colourRed');
        return taOptions;
    }]);
    
   $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$modal', 'taToolFunctions', 'toastr', function (taRegisterTool, taOptions, $modal, taToolFunctions, toastr) {
        taRegisterTool('myUploadImage', {
            iconclass: "fa fa-image",
            tooltiptext: "Insert Image",
            action: function (deferred) {
                $modal.open({
                    templateUrl: './src/app/views/embed_image_modal.html',
                    controller: function($scope, $modalInstance, Upload, baseUrl, $cookies) {
                        $scope.image = '';
                        $scope.progress = 0;
                        $scope.test = function(file){
                            console.log(file);
                        };
                        $scope.upload = function(file, validate){
                            if(validate.length < 1){
                                Upload.upload({
                                    url: baseUrl+'/article/photo/upload',
                                    fields: {'access_token': $cookies.get('access_token')},
                                    file: file,
                                    method: 'POST'
                                }).progress(function (evt) {
                                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                                }).success(function (response) {
                                    $scope.progress = 0;
                                    $scope.image = response.photo_url;
                                });
                            }
                            else{
                                toastr.error("Acceptable images are: JPG, TIF, PNG, and GIF.", "File Error");
                            }
                        };
                    
                        $scope.insert = function(){
                            $modalInstance.close($scope.image);
                        };
                    }
                }).result.then(
                    function (result) {
                        var concat = '<img src="'+result+'" style="max-width:650px">';
                        document.execCommand('insertHTML', false, concat);
                        //document.execCommand('insertImage', true, result);
                        deferred.resolve();
                    },
                    function () {
                        deferred.resolve();
                    }
                );
                return false;
            },
            onElementSelect: {
            	element: 'img',
            	action: taToolFunctions.imgOnSelectAction
            }
        });
        //taOptions.toolbar[1].push('myUploadImage');
        return taOptions;
    }]);
    
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,    
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    })
    
    $authProvider.facebook({
      url: '/community/auth/facebook',
      clientId: '468461729999988',
      scope: ['email', 'public_profile', 'user_friends'],
      redirectUri: window.location.origin + '/community/'
    });
  
});
angular.module('mainApp')
.controller('create_featureCtrl', function ($scope, $location, $rootScope, baseUrl, Restangular, $q, Upload, $modal, $cookies, $routeParams) {
    var get_featureAPI = Restangular.all('/article/load');
    $scope.feature = {};
    $scope.feature.timestamp = formatDateTime(new Date());
    $scope.feature.type = 'feature';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    //If id is set in the url get that feature
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_featureAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.feature = response.article;
            
            //$scope.feature.long_desc = response.article.uncompiled_long_desc; //long desc needs to be uncompiled to edit (iframes)
      
            var converted_date = new Date(Date.parse($scope.feature.created_at));
			$scope.feature.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
})
angular.module('mainApp')
.controller('create_gigphotosCtrl', function($scope, $rootScope, $cookies, Restangular, baseUrl, $location, $routeParams, $q, Upload, toastr){
	var get_gigphotosAPI = Restangular.all('/gigphotos/load');
    
    $scope.gigphotos = {};
    $scope.gigphotos.photos = [];
    $scope.gigphotos.timestamp = formatDateTime(new Date());
    $scope.gigphotos.type = 'gigphotos';

    //confirm leaving the page
    $rootScope.confirm_location_change = true;
    
    $scope.sortableOptions = { 
        containerPositioning:'relative',
        additionalPlaceholderClass: 'col-sm-4',
        //containment: '#board',
        //restrict move across columns. move only within column.
        accept: function (sourceItemHandleScope, destSortableScope) {
            //console.log(sourceItemHandleScope.itemScope.sortableScope.$id + " " + destSortableScope.$id);
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        },
        orderChanged: function (event){
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    };
    
    $scope.multiSelectCredit = function(credit){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.credit = credit;
        });
    }
    $scope.multiSelectTitle = function(title){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.title = title;
        });
    }
    $scope.multiSelectCaption = function(caption){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.alt = caption;
        });
    }
    
    function sortOrderIds(gigphotos_photos){
        angular.forEach(gigphotos_photos, function(photo, index){
            photo.order_id = index;
        });
        return gigphotos_photos;
    }
    
    $scope.$watch('gigphotos_photos', function (photos) {
        if($scope.gigphotos_photos){
            $scope.gigphotos.photos.push.apply($scope.gigphotos.photos, photos);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    });
    
    //$scope.$watch('gigphotos.photos', function (photos) {
    //    $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
    //});
    
    $scope.deleteImage = function(image){
        if(image.size){ //check if image is a file
            $scope.gigphotos.photos.splice(image.order_id, 1);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
        if(image.id){ //if the image has an id its a url and not a file
            $scope.gigphotos.deleted_images.push({id: image.id}); //add the deleted id to array for proper deletion at the server
            $scope.gigphotos.photos.splice(image.order_id, 1);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    }
    
   
    //If id is set in the url get that gigphotos
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_gigphotosAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.gigphotos = response.article;
            $scope.gigphotos.photos = response.images;
            $scope.gigphotos.deleted_images = []; //to keep track of deleted images || photos.id
            $scope.gigphotos.existing_images = []; //to keep track of the existing url's for saved images and their order || photos.id and photos.order_id
            //$rootScope.$broadcast('elastic:adjust');
            var converted_date = new Date(Date.parse($scope.gigphotos.created_at));
			$scope.gigphotos.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    $scope.disableToastr = function(form){
        if(!form.$valid) {
            toastr.info('Please fill out all the fields and select photo','Info');
        }
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
});
angular.module('mainApp')
.controller('create_musicnewsCtrl', function ($scope, $location, $rootScope, baseUrl, Restangular, $q, Upload, $modal, $cookies, $routeParams) {
    var get_musicnewsAPI = Restangular.all('/musicnews/load');
    $scope.musicnews = {};
    $scope.musicnews.timestamp = formatDateTime(new Date());
    $scope.musicnews.type = 'musicnews';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    //If id is set in the url get that musicnews
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_musicnewsAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.musicnews = response.article;
            var converted_date = new Date(Date.parse($scope.musicnews.created_at));
			$scope.musicnews.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
})
angular.module('mainApp')
.controller('create_reviewCtrl', function ($scope, $location, $rootScope, baseUrl, Restangular, $q, Upload, $modal, $cookies, $routeParams) {
    var get_reviewAPI = Restangular.all('/review/load');
    $scope.review = {};
    $scope.review.timestamp = formatDateTime(new Date());
    $scope.review.type = 'review';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    //If id is set in the url get that review
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_reviewAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.review = response.article;
            var converted_date = new Date(Date.parse($scope.review.created_at));
			$scope.review.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
})
angular.module('mainApp')
.controller('create_slideshowCtrl', function($scope, $rootScope, $cookies, Restangular, baseUrl, $location, $routeParams, $q, Upload){
	var get_slideshowAPI = Restangular.all('/slideshow/load');
    
    $scope.slideshow = {};
    $scope.slideshow.photos = [];
    $scope.slideshow.timestamp = formatDateTime(new Date());
    $scope.slideshow.type = 'slideshow';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    $scope.sortableOptions = { 
        containerPositioning:'relative',
        additionalPlaceholderClass: 'col-sm-4',
        //clone: true,
        //containment: '#board',
        //restrict move across columns. move only within column.
        accept: function (sourceItemHandleScope, destSortableScope) {
            //console.log(sourceItemHandleScope.itemScope.sortableScope.$id + " " + destSortableScope.$id);
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        },
        orderChanged: function (event){
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    };
    
    $scope.multiSelectCredit = function(credit){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.credit = credit;
        });
    }
    $scope.multiSelectTitle = function(title){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.title = title;
        });
    }
    $scope.multiSelectCaption = function(caption){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.alt = caption;
        });
    }
    
    function sortOrderIds(slideshow_photos){
        angular.forEach(slideshow_photos, function(photo, index){
            photo.order_id = index;
        });
        return slideshow_photos;
    }
    
    $scope.$watch('slideshow_photos', function (photos) {
        if($scope.slideshow_photos){
            $scope.slideshow.photos.push.apply($scope.slideshow.photos, photos);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    });
    
    //$scope.$watch('slideshow.photos', function (photos) {
    //    $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
    //});
    
    $scope.deleteImage = function(image){
        if(image.size){ //check if image is a file
            $scope.slideshow.photos.splice(image.order_id, 1);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
        if(image.id){ //if the image has an id its a url and not a file
            $scope.slideshow.deleted_images.push({id: image.id}); //add the deleted id to array for proper deletion at the server
            $scope.slideshow.photos.splice(image.order_id, 1);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    }
    
   
    //If id is set in the url get that slideshow
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_slideshowAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.slideshow = response.article;
            $scope.slideshow.photos = response.images;
            $scope.slideshow.deleted_images = []; //to keep track of deleted images || photos.id
            $scope.slideshow.existing_images = []; //to keep track of the existing url's for saved images and their order || photos.id and photos.order_id
            //$rootScope.$broadcast('elastic:adjust');
            var converted_date = new Date(Date.parse($scope.slideshow.created_at));
			$scope.slideshow.timestamp = formatDateTime(converted_date);
        }, function(error){
            $rootScope.loading = false;
            console.log(error);
            $location.path('/');
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
	
	//For handling multiple uploads
    function uploads(token, slideshow_id, photos_url, files){
        var deferred = $q.defer();
        var queue = [];

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var promise = uploadImage(token, slideshow_id, photos_url, files[i]);
                queue.push(promise);
            }
        }

        $q.all(queue).then(
            function (data) {
                deferred.resolve(data)
            },
            function (err) {
                deferred.reject(err)
            }
        )

        return deferred.promise
    }
    //Called from uploads-function
    function uploadImage(token, slideshow_id, url, file) {
        var promise = Upload.upload({
            url: url,
            headers: {'Content-Type': "multipart/form-data"},
            method: 'POST',
            fields: {access_token: token, slideshow_id: slideshow_id},
            file: file
        }).progress(function (evt) {
            var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            file.progress = progressPercentage;
        }).success(function (data, status, headers, config) {
            file.result = 'file ' + config.file.name + ' uploaded. Response: ' + data;
            $scope.test_picture = data.photo_url;
        });
        return promise;
    };
});
angular.module('mainApp')
.controller('homeCtrl', function ($scope, $location, $rootScope, baseUrl, $modal, $cookies, Restangular, $window, $route) {
	var load_homepagearticlesAPI = Restangular.all('/public/articles/homepage');
	var top_postsAPI = Restangular.all('public/articles/topposts');
	var top_usersAPI = Restangular.all('public/articles/topusers');
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;
	
	$scope.active_tab = {latest: false, featured: false, top_users: false, top_posts: false}
	$scope.articles = [];
	$scope.pagination = {received: 0, requested: 5, maxed: true};
	
	evaluateTab($route.current.$$route.activetab);
	
	$scope.loadMore = function(page){
		getArticles(page);
	}
	$scope.redirectHome = function(path){
		$location.path(path);
	}
	$scope.loadPostsList = function(list){
		switch(list) {
			case 'Week':
				$scope.loaded_list = $scope.top_posts.top_posts_week;
				break;
			case 'Month':
				$scope.loaded_list = $scope.top_posts.top_posts_month;
				break;
			case 'Year':
				$scope.loaded_list = $scope.top_posts.top_posts_year;
				break;
		}
	}
	$scope.loadUsersList = function(list){
		switch(list) {
			case 'Week':
				$scope.loaded_list = $scope.top_users.top_users_week;
				break;
			case 'Month':
				$scope.loaded_list = $scope.top_users.top_users_month;
				break;
			case 'Year':
				$scope.loaded_list = $scope.top_users.top_users_year;
				break;
		}
	}
	$scope.forwardToArticle = function(url){
		window.open(url, '_blank');
	}
		
	function evaluateTab(tab){
		$scope.active_tab = {latest: false, featured: false, top_users: false, top_posts: false}
		switch(tab) {
			case 'latest':
				$scope.active_tab.latest = true;
				getArticles($scope.pagination);
				break;
			case 'featured':
				$scope.active_tab.featured = true;
				break;
			case 'top_users':
				$scope.active_tab.top_users = true;
				getTopUsers();
				break;
			case 'top_posts':
				$scope.active_tab.top_posts = true;
				getTopPosts();
				break;
			default:
				$scope.active_tab.latest = true;
				getArticles($scope.pagination);
				break;
		}
	}
	
	function getArticles(page){
		if(!$rootScope.loading && $scope.active_tab.latest){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			var params = {number_of_articles_received: page.received, number_of_articles_requested: page.requested};
			load_homepagearticlesAPI.post(params).then(function(response){
				//console.log(page);
				page.maxed = false;
				$rootScope.loading = false;
				for (var i=0; i<response.articles.length; i++) {
					$scope.articles.push(response.articles[i]);
				}
				page.received += response.articles.length;
				if(page.requested > response.articles.length || response.articles.length == 0){
					page.maxed = true;
				}
			},function(error){
				console.log(error);
				$rootScope.loading = false;
			}).finally(function(){
				$rootScope.loading_background = '';
			});
		}
	}
	function getTopPosts(){
		if(!$rootScope.loading && $scope.active_tab.top_posts){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			top_postsAPI.post().then(function(response){
				$scope.top_posts = response.top_posts;
				$scope.loaded_list = response.top_posts.top_posts_month;
			},function(error){
				console.log(error);
			}).finally(function(){
				$rootScope.loading = false;
				$rootScope.loading_background = '';
			});
		}
	}
	function getTopUsers(){
		if(!$rootScope.loading && $scope.active_tab.top_users){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			top_usersAPI.post().then(function(response){
				$scope.top_users = response.top_users;
				$scope.loaded_list = response.top_users.top_users_month;
			},function(error){
				console.log(error);
			}).finally(function(){
				$rootScope.loading = false;
				$rootScope.loading_background = '';
			});
		}
	}
});
angular.module('mainApp')
.controller('mainCtrl', function($scope, $rootScope, $cookies, $modal, $location, Restangular, taApplyCustomRenderers, $q, Upload, baseUrl, toastr, $window){
	var save_featureAPI = Restangular.all('/article/save');
	var save_reviewAPI = Restangular.all('/review/save');
	var save_slideshowAPI = Restangular.all('/slideshow/save');
	var save_gigphotosAPI = Restangular.all('/gigphotos/save');
	var save_musicnewsAPI = Restangular.all('/musicnews/save');
	var save_profileInfoAPI = Restangular.all('/account/info/save');
	var save_passwordAPI = Restangular.all('/account/password/change');
	var publish_articleAPI = Restangular.all('/article/publish');
	var unpublish_articleAPI = Restangular.all('/article/unpublish');
	var username_takenAPI = Restangular.all('/account/username/check');
	var save_usernameAPI = Restangular.all('/account/username/set');
	var check_tokenAPI = Restangular.all('/account/istokenvalid');
	var signupAPI = Restangular.all('web/signup');
	var get_tagsAPI = Restangular.all('/article/tags/get');
	
	//get_tagsAPI.post({access_token: $cookies.get('access_token')}).then(function(result){
	//	console.log(result.tags);
	//})
	$rootScope.profile_photo = $cookies.get('profile_photo');
	$rootScope.access_token = $cookies.get('access_token');
	
	//Confirm leaving article-pages
	$scope.$on('$locationChangeStart', function(event, next, current) {
        if($rootScope.confirm_location_change){
            var answer = confirm("Are you sure you want to leave this page? \nAny unsaved changes will be lost.");
            if (!answer) {
                event.preventDefault();
            } 
        }
    });

	//Check if access_token is valid on page-change. Else redirect to homepage
	$scope.$on('$routeChangeStart', function(next, current) {
		if($cookies.get('access_token')){
			check_tokenAPI.post({access_token: $cookies.get('access_token')}).then(function(response){
				//access token valid
			},function(error){
				$rootScope.loading = false;
				$cookies.remove('access_token');
				$location.path('/');
			});
		}
		else{
			if(current.activetab != 'latest' && current.activetab != 'top_posts' && current.activetab != 'top_users'){
				$location.path('/');
			}
		}
		
		//handles if you publish from create-articles to route to the published tab under my-articles
		if(current.activetab != 'my-articles'){
			$rootScope.publish_route = false;
		}
	});
	
	$scope.log = function(log){
		console.log(log);
	}
	$scope.validateImage = function(file, photos){
		if(file.length > 0){
			toastr.error("Acceptable images are: JPG, TIF, PNG, and GIF.", "File Error");
		}
	}
	$scope.signUp = function(signup){
		$rootScope.loading = true;
		var params = {first_name: signup.firstname,last_name: signup.lastname, email: signup.email, username: signup.username, password: signup.password};
        signupAPI.post(params).then(function(response){
			$scope.openConfirmSignupModal(signup.email);
        }, function(error){
            console.log(error);
        }).finally(function(){
			$rootScope.loading = false;
		});
	}
	$scope.logout = function(){
		$rootScope.access_token = '';
		$cookies.remove('access_token');
		$location.path('/');
	}
	$scope.goViewArticle = function(id, title){
		$location.path('/article/'+id +'/'+title);
	}
	$scope.openCreatePostModal = function(data){
		var templateUrl = './src/app/views/create_post_modal.html';
		var size = 'md';
		var windowClass = 'create-post-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openPublishtModal = function(data){
		var templateUrl = './src/app/views/publish_modal.html';
		var size = 'md';
		var windowClass = 'publish-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openCropModal = function(data){
		var templateUrl = './src/app/views/crop_modal.html';
		var size = 'md';
		var windowClass = 'crop-modal';
		var backdrop = 'static';
		var keyboard = false;
		$modal.open({
			animation: true,
			backdrop: backdrop,
			keyboard: keyboard,
			templateUrl: templateUrl,
			controller: 'ModalCtrl',
			size: size,
			windowClass: windowClass,
			resolve: {
				data: function () {
					return data;
				}
			}
    	}).result.then(function(result) {
			if(result){
				data.photo = result.photo_cropped;
			}
		});
	}
	$scope.openLoginModal = function(data){
		var templateUrl = './src/app/views/login_modal.html';
		var size = 'md';
		var windowClass = 'login-modal';
		openModal(size, templateUrl, windowClass, data);
	}
    $scope.openPreviewModal = function(data){
		var templateUrl = './src/app/views/preview_article_modal.html';
		var size = 'md';
		var windowClass = 'preview-article-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openPreviewSlideshowModal = function(data){
		var templateUrl = './src/app/views/preview_slideshow_modal.html';
		var size = 'lg';
		var windowClass = 'preview-slideshow-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openImageLoadingModal = function(data){
		var templateUrl = './src/app/views/image_loading_modal.html';
		var size = 'md';
		var windowClass = 'preview-article-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openConfirmSignupModal = function(data){
		var templateUrl = './src/app/views/confirm_signup_modal.html';
		var size = 'md';
		var windowClass = 'confirm-signup-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	
	$scope.redirectNewTab = function(link){
        $window.open(link, '_blank');
    };
	
	$scope.saveUsername = function(access_token, profile){
		if(profile.username == profile.original_username){
			toastr.info('Username hasnt changed', 'Username');
		}
		else{
			var params = {username: profile.username};
			username_takenAPI.post(params).then(function(response){
				params = {access_token: access_token, username: profile.username};
				save_usernameAPI.post(params).then(function(response){
					profile.original_username = profile.username;
					toastr.success('Username saved!', 'Username');
				}, function(error){
					console.log(error);
					toastr.error(error.data.message, 'Username');
				});
			}, function(error){
				console.log(error);
				toastr.error(error.data.message, 'Username');
			});
		}
	}
	
	$scope.publishArticle = function(access_token, article, publish_from_edit){
		console.log(article);
		var params = {access_token: access_token, article_id: article.id}
		publish_articleAPI.post(params).then(function(response){
			article.is_draft = 0;
			article.link_to_article = response.article.link_to_article;
			article.facebook_link = response.article.facebook_link;
			$rootScope.publish_route = true;
			if(!publish_from_edit){
				$scope.openPublishtModal(article);
			}
			toastr.success('Your article has been published!', 'Article');
		}, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Article');
        }).finally(function(){
			
		});
	}
	
	$scope.unpublishArticle = function(access_token, article){
		var params = {access_token: access_token, article_id: article.id}
		unpublish_articleAPI.post(params).then(function(response){
			article.is_draft = 1;
			toastr.success('Your article has been unpublished!', 'Article');
		}, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Article');
        }).finally(function(){
			
		});
	}
	
	$scope.changePassword = function(access_token, old_password, new_password){
		var params = {access_token: access_token, old_password: old_password, new_password: new_password}
		save_passwordAPI.post(params).then(function(response){
			toastr.success('Password-change successfull!', 'Settings');
        }, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Settings');
        }).finally(function(){
			
		});
	}
	
	$scope.saveProfile = function(access_token, profile){
		$rootScope.loading = true;
		if(!profile.photo_url){
			profile.photo_url = '';
		}
		var photo = [profile.photo_url];
		var params = {access_token: access_token, email: profile.email, first_name: profile.first_name, last_name: profile.last_name, country: profile.country, gender: profile.gender,
		bio: profile.bio, website: profile.website};
        save_profileInfoAPI.post(params).then(function(response){
			if(photo[0].length > 3000){ //if the photo is a file
				photo[0] = Upload.dataUrltoBlob(photo[0]);
				var url = baseUrl + '/account/photo/save';
        		var promise_a = uploadMultilpleImages($cookies.get('access_token'), '', url, photo);
				$q.all([promise_a]).then(function(response){
					profile.photo_url = response[0][0].data.photo_url;
					$cookies.put('profile_photo', profile.photo_url);
					$rootScope.profile_photo = $cookies.get('profile_photo');
					$rootScope.loading = false;
					toastr.success('Your profile has been saved!', 'Settings');
				});
			}
			else{
				$rootScope.loading = false;
				toastr.success('Your profile has been saved!', 'Settings');
			}	
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
		
	//Save Feature API-call
	$scope.saveFeature = function(access_token, feature, publish){
		//var uncompiled_long_desc = feature.long_desc;
		//feature.long_desc = taApplyCustomRenderers(feature.long_desc);
		
		var data = {}
		data.publish = publish;
		data.article = feature;
		if(feature.photo && feature.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [feature.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(feature.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(feature.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: feature.title, short_desc: feature.short_desc, long_desc: feature.long_desc, article_id: feature.id, photographer: feature.photographer, tags: {}};
        save_featureAPI.post(params).then(function(response){
			feature.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(feature.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, feature.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					feature.photo = '';
					feature.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, feature, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Review API-call
	$scope.saveReview = function(access_token, review, publish){
		//review.long_desc = taApplyCustomRenderers(review.long_desc);
		var data = {}
		data.publish = publish;
		data.article = review;
		if(review.photo && review.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [review.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(review.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(review.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: review.title, short_desc: review.short_desc, long_desc: review.long_desc, article_id: review.id, photographer: review.photographer, tags: {}};
        save_reviewAPI.post(params).then(function(response){
			review.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(review.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, review.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					review.photo = '';
					review.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}	
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, review, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Musicnews API-call
	$scope.saveMusicnews = function(access_token, musicnews, publish){
		//musicnews.long_desc = taApplyCustomRenderers(musicnews.long_desc);
		var data = {}
		data.publish = publish;
		data.article = musicnews;
		if(musicnews.photo && musicnews.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [musicnews.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(musicnews.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(musicnews.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: musicnews.title, short_desc: musicnews.short_desc, long_desc: musicnews.long_desc, article_id: musicnews.id, photographer: musicnews.photographer, tags: {}};
        save_musicnewsAPI.post(params).then(function(response){
			musicnews.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(musicnews.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, musicnews.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					musicnews.photo = '';
					musicnews.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, musicnews, false);
			}		
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Gigphotos
	$scope.saveGigphotos = function(access_token, gigphotos, publish){
		var data = {}; //data for modal
		data.publish = publish;
		data.article = gigphotos;
		data.files_to_send = [];

		angular.forEach(gigphotos.photos, function(photo, index){
			if(photo.size){ // if the photo is a file
            	data.files_to_send.push(photo);
			}
			else{
				gigphotos.existing_images.push(photo);
			}
        });
		
		if(gigphotos.photo){ //if the photo is file
			if(gigphotos.photo.size){ //if the photo is an uncropped file
				gigphotos.photo.is_cover_photo = true;
			}
			else{ //if the photo is a cropped fle
				gigphotos.photo = Upload.dataUrltoBlob(gigphotos.photo);
				gigphotos.photo.is_cover_photo = true;
			}
			data.files_to_send.push(gigphotos.photo);	
			$scope.openImageLoadingModal(data);	
		}
		else if(data.files_to_send.length < 1){//if there are no files to send
			$rootScope.loading = true;
		}
		else{
			$scope.openImageLoadingModal(data);	// if the cover-photo hasnt changed
		}
		var params = {access_token: access_token, title: gigphotos.title, short_desc: gigphotos.short_desc, long_desc: gigphotos.long_desc, article_id: gigphotos.id, tags: {}, 
		deleted_images: gigphotos.deleted_images, existing_images: gigphotos.existing_images, photographer: gigphotos.photographer};
        save_gigphotosAPI.post(params).then(function(response){
			gigphotos.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Gig has been saved!', 'Gigphotos');
			if(data.files_to_send.length > 0){
				var url = baseUrl + '/gigphotos/photo/upload';
        		var promise_a = uploadMultilpleImages(access_token, gigphotos.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					toastr.success('Your Gig-photos have been saved!', 'Gigphotos');
				}, function (error) {
					data.file_sent = true;
					console.log('herpaderp');
					toastr.error('Some of you Gig-photos didnt upload.', 'Gigphotos');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, gigphotos, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Slideshow API-call
	$scope.saveSlideshow = function(access_token, slideshow, publish){
		var data = {}; //data for modal
		data.publish = publish;
		data.article = slideshow;
		data.files_to_send = [];
		//data.files_to_send = slideshow.photos.slice();
		
		angular.forEach(slideshow.photos, function(photo, index){
			if(photo.size){ // if the photo is a file
            	data.files_to_send.push(photo);
			}
			else{
				slideshow.existing_images.push(photo);
			}
        });
		
		if(slideshow.photo){ //if the photo is file
			if(slideshow.photo.size){ //if the photo is an uncropped file
				slideshow.photo.is_cover_photo = true;
			}
			else{ //if the photo is a cropped fle
				slideshow.photo = Upload.dataUrltoBlob(slideshow.photo);
				slideshow.photo.is_cover_photo = true;
			}
			data.files_to_send.push(slideshow.photo);	
			$scope.openImageLoadingModal(data);	
		}
		else if(data.files_to_send.length < 1){//if there are no files to send
			$rootScope.loading = true;
		}
		else{
			$scope.openImageLoadingModal(data);	// if the cover-photo hasnt changed
		}
		var params = {access_token: access_token, title: slideshow.title, short_desc: slideshow.short_desc, long_desc: slideshow.long_desc, article_id: slideshow.id, tags: {}, 
		deleted_images: slideshow.deleted_images, existing_images: slideshow.existing_images, photographer: slideshow.photographer};
        save_slideshowAPI.post(params).then(function(response){
			slideshow.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Slideshow has been saved!', 'Slideshow');
			if(data.files_to_send.length > 0){
				var url = baseUrl + '/slideshow/photo/upload';
        		var promise_a = uploadMultilpleImages(access_token, response.article_id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					toastr.success('Your Slideshow-photos have been saved!', 'Slideshow');
				}, function (error) {
					data.file_sent = true;
					toastr.error('Some of you Slideshow-photos didnt upload.', 'Slideshow');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, slideshow, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	function openModal(size, templateUrl, windowClass, data) {
		var backdrop = 'true';
		var keyboard = true;
		if(!data){
			data = {};
		}
		data.templateUrl = templateUrl;
		if(templateUrl == './src/app/views/image_loading_modal.html'){
			backdrop = 'static';
			keyboard = false;
		}
		$modal.open({
			animation: true,
			backdrop: backdrop,
			keyboard: keyboard,
			templateUrl: templateUrl,
			controller: 'ModalCtrl',
			size: size,
			windowClass: windowClass,
			resolve: {
				data: function () {
					return data;
				}
			}
    	});
	}
	//For handling multiple uploads
    function uploadMultilpleImages(token, article_id, photos_url, files){
        var deferred = $q.defer();
        var queue = [];

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var promise = uploadImage(token, article_id, photos_url, files[i]);
                queue.push(promise);
            }
        }

        $q.all(queue).then(
            function (data) {
                deferred.resolve(data)
            },
            function (err) {
                deferred.reject(err)
            }
        )

        return deferred.promise
    }
    //Called from uploads-function
    function uploadImage(token, article_id, url, file) {
		var fields = {access_token: token, article_id: article_id};
		if(url == (baseUrl + '/slideshow/photo/upload')){
			fields = {access_token: token, article_id: article_id, order_id: file.order_id, credit: file.credit, title: file.title, alt: file.alt, is_cover_photo: file.is_cover_photo};
		}
		if(url == (baseUrl + '/gigphotos/photo/upload')){
			fields = {access_token: token, article_id: article_id, order_id: file.order_id, is_cover_photo: file.is_cover_photo};
		}
        var promise = Upload.upload({
            url: url,
            headers: {'Content-Type': "multipart/form-data"},
            method: 'POST',
            fields: fields,
            file: file
        }).progress(function (evt) {
            var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            file.progress = progressPercentage;
        }).success(function (data, status, headers, config) {
            file.result = 'file ' + config.file.name + ' uploaded. Response: ' + data;
            //$scope.test_picture = data.photo_url;
        }).catch(function(error){
			console.log(error);
			file.error = error.data.error;
		});
        return promise;
    };
})
.controller('ModalCtrl', function ($scope, $modalInstance, data, Restangular, $rootScope, $location, Upload, $cookies, $route, toastr, $auth) {
	var loginAPI = Restangular.all('web/login');
	var forgotpasswordAPI = Restangular.all('web/forgotemail');
	var facebookSigninAPI = Restangular.all('web/fblogin');
	$scope.access_token = $cookies.get('access_token');
	
	//Login-modal code
	$scope.signIn = function(email, password){
		$scope.login_error_msg = '';
		var params = {email: email, password: password};
        loginAPI.post(params).then(function(response){
			$cookies.put('access_token', response.access_token);
			$rootScope.access_token = response.access_token;
			$cookies.put('profile_photo', response.profile_photo);
			$rootScope.profile_photo = $cookies.get('profile_photo');
			$modalInstance.close();
			$location.path('/my-articles');
        }, function(error){
			$scope.login_error_msg = error.data.message;
            console.log(error);
        });
	}
	
	$scope.facebookSignup = function(provider) {
      	$auth.authenticate(provider).then(function(response) {
			$modalInstance.close();
			$rootScope.loading = true;
			var params = {code: response.config.data.code, redirect_uri: response.config.data.redirectUri};
			facebookSigninAPI.post(params).then(function(response){
				$rootScope.loading = false;
				console.log(response);
				$cookies.put('access_token', response.access_token);
				$rootScope.access_token = response.access_token;
				$cookies.put('profile_photo', response.profile_photo);
				$rootScope.profile_photo = $cookies.get('profile_photo');
				$location.path('/my-articles');
			}, function(error){
				$rootScope.loading = false;
				$scope.login_error_msg = error.data.message;
				console.log(error);
			});
    	}).catch(function(response) {
			$rootScope.loading = false;
          	console.log(response);
        });
    };
	
	$scope.resetPassword = function(email){
		$modalInstance.close();
		$rootScope.loading = true;
		var params = {email: email};
        forgotpasswordAPI.post(params).then(function(response){
			$rootScope.loading = false;
			alert("An email has been sent to "+email+" with details on how to reset your password");
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Create-Post-modal code
	$scope.forwardFeature = function(){
		$modalInstance.close();
		$location.path('/create-feature');
	}
	$scope.forwardReview = function(){
		$modalInstance.close();
		$location.path('/create-review');
	}
	$scope.forwardGigPhotos = function(){
		$modalInstance.close();
		$location.path('/create-gigphotos');
	}
	$scope.forwardSlideshow = function(){
		$modalInstance.close();
		$location.path('/create-slideshow');
	}
	$scope.forwardMusicnews = function(){
		$modalInstance.close();
		$location.path('/create-musicnews');
	}
	
	//Preview-Article-modal code
	$scope.article = _.cloneDeep(data);
	$scope.article.photo_cropped = '';
	
	//loading modal code
	$scope.modal_data = data;
	
	//slideshow modal
	if(data.templateUrl == './src/app/views/preview_slideshow_modal.html'){

		if($scope.article.photos.length > 0 && $scope.article.cover_photo){
			if($scope.article.photos[0]['file_name']){
				$scope.article.photos.unshift({file_name: $scope.article.cover_photo});
			}
			if($scope.article.photos[0]['size']){
				$scope.article.photos.unshift($scope.article.cover_photo);
			}
		}
		else{
			toastr.info('Please select at least one photo and cover-photo', 'Images');
		}
	}
	
	
	
	$scope.ok = function () {
		if($scope.modal_data.file_sent){
			if($scope.modal_data.article.post_type_id){ //if the article isnt new
				$modalInstance.close();
				$route.reload();
			}
			else{//the article is new
				$rootScope.confirm_location_change = false;
				$location.path('/edit-'+$scope.modal_data.article.type+'/'+$scope.modal_data.article.id);
				$modalInstance.close();
			} 
			
		}
	};
	$scope.ok_signup = function () {
		$modalInstance.close();
		$route.reload();
	};
	$scope.cancel = function () {
		$modalInstance.dismiss();
		$route.reload();
	};
	$scope.ok_modal = function () {
		$modalInstance.close($scope.article);
	};
	$scope.cancel_modal = function () {
		$modalInstance.close();
	};
	$scope.ok_publish_modal = function(){
		$rootScope.confirm_location_change = false;
		$modalInstance.close();
		$location.path('/my-articles');	
	}
	
	
	
	if(data.templateUrl == './src/app/views/test_modal.html'){
		$scope.image = 'img/default.png';
	
		$scope.progress = 0;
		$scope.files = [];
	}
	
	$scope.upload = function(){
		Upload.upload({
			url: 'api/upload',
			fields: {'dir': 'img/uploads/'},
			file: $scope.files[0],
			method: 'POST'
		}).progress(function (evt) {
			$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
		}).success(function (data) {
			$scope.progress = 0;
			$scope.image = data.dir+data.filename;
		});
	};

	$scope.insert = function(){
		$modalInstance.close($scope.image);
	};
	
	$scope.validateEmail = function(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
});
angular.module('mainApp')
.controller('navCtrl', function ($scope, $location, $rootScope, $modal, $cookies) {
	
})
angular.module('mainApp')
.controller('profileCtrl', function ($scope, $location, $rootScope, baseUrl, $modal, Restangular, $cookies) {
	var my_articlesAPI = Restangular.all('/account/articles/get');
	var loadProfileAPI = Restangular.all('/account/info/load');
	
	loadProfile($cookies.get('access_token'));
	$scope.articles = {};
	getArticles($cookies.get('access_token'));
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;

	function getArticles(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        my_articlesAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.articles = response.articles;
        }, function(error){
            console.log(error);
        });
	}
	//returns array of specific length for mock ng-repeat
	$scope.getNumber = function(num) {
        return new Array(num);   
	}
	
	function loadProfile(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        loadProfileAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.user = response.user;
			$rootScope.profile_photo = response.user.photo_url;
        }, function(error){
            console.log(error);
        });
	}
});
angular.module('mainApp')
.controller('profile_settingsCtrl', function($scope, $rootScope, $location, $cookies, Restangular, $route, toastr){
	var my_articlesAPI = Restangular.all('/account/articles/get');
	var deleteDraftAPI = Restangular.all('/article/delete');
	var loadProfileAPI = Restangular.all('/account/info/load');
	
	$scope.profile = {};
	$scope.profile.photo_url = '';
	$scope.pagination = {received: 0, requested: 5, maxed: true};
	$scope.articles = [];
	//getArticles($cookies.get('access_token'));
	loadProfile($cookies.get('access_token'));
	$scope.active_tab = {};
	$scope.draft_publish = "";
	evaluateActiveTab();
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;
	
	function evaluateActiveTab(draft_publish){
		$scope.draft_publish = draft_publish;
		if($route.current.activetab == 'settings'){
			$scope.active_tab.settings = true;		
		}
		if($route.current.activetab == 'my-articles'){
			$scope.active_tab.myarticles = true;
			if(draft_publish == 'publish' || $rootScope.publish_route){
				$scope.draft_publish = 'publish';
				getArticles($cookies.get('access_token'), 0, $scope.pagination);
			}
			else{
				$scope.draft_publish = 'draft';
				getArticles($cookies.get('access_token'), 1, $scope.pagination);
			}
		}
	}
	
	$scope.loadMore = function(page, draft_publish){
		if(draft_publish == 'publish'){
			getArticles($cookies.get('access_token'), 0, $scope.pagination);
		}
		else{
			getArticles($cookies.get('access_token'), 1, $scope.pagination);
		}
	}
	
	$scope.evaluateActiveTab = function(draft_publish){
		$rootScope.publish_route = false;
		$scope.articles = [];
		$scope.pagination = {received: 0, requested: 5, maxed: true};
		evaluateActiveTab(draft_publish);
	}

	$scope.deleteDraft = function(article, access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token, article_id: article.id};
        deleteDraftAPI.post(params).then(function(response){
			$rootScope.loading = false;
			toastr.success('Your draft has been deleted','Draft');
            $route.reload();
			$rootScope.publish_route = false;
        }, function(error){
            console.log(error);
        });
	}
	
	$scope.goToMyArticles = function(){
		$location.path('/my-articles');
	}
	$scope.goToSettings = function(){
		$location.path('/settings');
	}
	
	function loadProfile(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        loadProfileAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.profile = response.user;
			$scope.profile.original_username = response.user.username;
			$rootScope.profile_photo = response.user.photo_url;
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	function getArticles(access_token, is_draft, page){
		$rootScope.loading_background = 'transparent';
		$rootScope.loading = true;
		var params = {access_token: access_token, is_draft: is_draft, number_of_articles_received: page.received, number_of_articles_requested: page.requested};
        my_articlesAPI.post(params).then(function(response){
			$rootScope.loading = false;
			page.maxed = false;
				$rootScope.loading = false;
				for (var i=0; i<response.articles.length; i++) {
					$scope.articles.push(response.articles[i]);
				}
				page.received += response.articles.length;
				if(page.requested > response.articles.length || response.articles.length == 0){
					page.maxed = true;
				}
        }, function(error){
            console.log(error);
			$rootScope.loading = false;
        }).finally(function(){
			$rootScope.loading_background = '';
		});
	}
});
angular.module('mainApp')
.controller('view_articleCtrl', function($scope, $rootScope){
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;
	
});
angular.module('mainApp')
//For closing dropdown menus
.directive('clickOutside', function ($document) {
    return {
        restrict: 'A',
        scope: {
            clickOutside: '&'
        },
        link: function (scope, el, attr) {

            $document.on('click', function (e) {
                if (el !== e.target && !el[0].contains(e.target)) {
                    scope.$apply(function () {
                        scope.$eval(scope.clickOutside);
                    });
                }
            });
        }
    }

})
.directive('ssEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ssEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
})
.directive('autoFocus', function($timeout) {
    return {
        restrict: 'AC',
        link: function(_scope, _element) {
            $timeout(function(){
                _element[0].focus();
            }, 0);
        }
    };
})
// part of elastic directive
.constant('msdElasticConfig', {
  append: ''
})
.directive('elastic', [
  '$timeout', '$window', 'msdElasticConfig',
  function($timeout, $window, config) {
    'use strict';

    return {
      require: 'ngModel',
      restrict: 'A, C',
      link: function(scope, element, attrs, ngModel) {

        // cache a reference to the DOM element
        var ta = element[0],
            $ta = element;

        // ensure the element is a textarea, and browser is capable
        if (ta.nodeName !== 'TEXTAREA' || !$window.getComputedStyle) {
          return;
        }

        // set these properties before measuring dimensions
        $ta.css({
          'overflow': 'hidden',
          'overflow-y': 'hidden',
          'word-wrap': 'break-word'
        });

        // force text reflow
        var text = ta.value;
        ta.value = '';
        ta.value = text;

        var append = attrs.msdElastic ? attrs.msdElastic.replace(/\\n/g, '\n') : config.append,
            $win = angular.element($window),
            mirrorInitStyle = 'position: absolute; top: -999px; right: auto; bottom: auto;' +
                              'left: 0; overflow: hidden; -webkit-box-sizing: content-box;' +
                              '-moz-box-sizing: content-box; box-sizing: content-box;' +
                              'min-height: 0 !important; height: 0 !important; padding: 0;' +
                              'word-wrap: break-word; border: 0;',
            $mirror = angular.element('<textarea aria-hidden="true" tabindex="-1" ' +
                                      'style="' + mirrorInitStyle + '"/>').data('elastic', true),
            mirror = $mirror[0],
            taStyle = getComputedStyle(ta),
            resize = taStyle.getPropertyValue('resize'),
            borderBox = taStyle.getPropertyValue('box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-moz-box-sizing') === 'border-box' ||
                        taStyle.getPropertyValue('-webkit-box-sizing') === 'border-box',
            boxOuter = !borderBox ? {width: 0, height: 0} : {
                          width:  parseInt(taStyle.getPropertyValue('border-right-width'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-right'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-left'), 10) +
                                  parseInt(taStyle.getPropertyValue('border-left-width'), 10),
                          height: parseInt(taStyle.getPropertyValue('border-top-width'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-top'), 10) +
                                  parseInt(taStyle.getPropertyValue('padding-bottom'), 10) +
                                  parseInt(taStyle.getPropertyValue('border-bottom-width'), 10)
                        },
            minHeightValue = parseInt(taStyle.getPropertyValue('min-height'), 10),
            heightValue = parseInt(taStyle.getPropertyValue('height'), 10),
            minHeight = Math.max(minHeightValue, heightValue) - boxOuter.height,
            maxHeight = parseInt(taStyle.getPropertyValue('max-height'), 10),
            mirrored,
            active,
            copyStyle = ['font-family',
                          'font-size',
                          'font-weight',
                          'font-style',
                          'letter-spacing',
                          'line-height',
                          'text-transform',
                          'word-spacing',
                          'text-indent'];

        // exit if elastic already applied (or is the mirror element)
        if ($ta.data('elastic')) {
          return;
        }

        // Opera returns max-height of -1 if not set
        maxHeight = maxHeight && maxHeight > 0 ? maxHeight : 9e4;

        // append mirror to the DOM
        if (mirror.parentNode !== document.body) {
          angular.element(document.body).append(mirror);
        }

        // set resize and apply elastic
        $ta.css({
          'resize': (resize === 'none' || resize === 'vertical') ? 'none' : 'horizontal'
        }).data('elastic', true);

        /*
          * methods
          */

        function initMirror() {
          var mirrorStyle = mirrorInitStyle;

          mirrored = ta;
          // copy the essential styles from the textarea to the mirror
          taStyle = getComputedStyle(ta);
          angular.forEach(copyStyle, function(val) {
            mirrorStyle += val + ':' + taStyle.getPropertyValue(val) + ';';
          });
          mirror.setAttribute('style', mirrorStyle);
        }

        function adjust() {
          var taHeight,
              taComputedStyleWidth,
              mirrorHeight,
              width,
              overflow;

          if (mirrored !== ta) {
            initMirror();
          }

          // active flag prevents actions in function from calling adjust again
          if (!active) {
            active = true;

            mirror.value = ta.value + append; // optional whitespace to improve animation
            mirror.style.overflowY = ta.style.overflowY;

            taHeight = ta.style.height === '' ? 'auto' : parseInt(ta.style.height, 10);

            taComputedStyleWidth = getComputedStyle(ta).getPropertyValue('width');

            // ensure getComputedStyle has returned a readable 'used value' pixel width
            if (taComputedStyleWidth.substr(taComputedStyleWidth.length - 2, 2) === 'px') {
              // update mirror width in case the textarea width has changed
              width = parseInt(taComputedStyleWidth, 10) - boxOuter.width;
              mirror.style.width = width + 'px';
            }

            mirrorHeight = mirror.scrollHeight;

            if (mirrorHeight > maxHeight) {
              mirrorHeight = maxHeight;
              overflow = 'scroll';
            } else if (mirrorHeight < minHeight) {
              mirrorHeight = minHeight;
            }
            mirrorHeight += boxOuter.height;
            ta.style.overflowY = overflow || 'hidden';

            if (taHeight !== mirrorHeight) {
              scope.$emit('elastic:resize', $ta, taHeight, mirrorHeight);
              ta.style.height = mirrorHeight + 'px';
            }

            // small delay to prevent an infinite loop
            $timeout(function() {
              active = false;
            }, 1, false);

          }
        }

        function forceAdjust() {
          active = false;
          adjust();
        }

        /*
          * initialise
          */

        // listen
        if ('onpropertychange' in ta && 'oninput' in ta) {
          // IE9
          ta['oninput'] = ta.onkeyup = adjust;
        } else {
          ta['oninput'] = adjust;
        }

        $win.bind('resize', forceAdjust);

        scope.$watch(function() {
          return ngModel.$modelValue;
        }, function(newValue) {
          forceAdjust();
        });

        scope.$on('elastic:adjust', function() {
          initMirror();
          forceAdjust();
        });

        $timeout(adjust, 0, false);

        /*
          * destroy
          */

        scope.$on('$destroy', function() {
          $mirror.remove();
          $win.unbind('resize', forceAdjust);
        });
      }
    };
  }
])
.directive('iframeSetDimensionsOnload', [function(){
return {
    restrict: 'A',
    link: function(scope, element, attrs){
        element.on('load', function(){
            /* Set the dimensions here, 
               I think that you were trying to do something like this: */
               var iFrameHeight = element[0].contentWindow.document.body.scrollHeight + 'px';
               var iFrameWidth = '100%';
               element.css('width', iFrameWidth);
               element.css('height', iFrameHeight);
        })
    }
}}])
.directive('countrySelect', ['$parse', function ($parse) {
    var countries = [
      "Afghanistan", "Aland Islands", "Albania", "Algeria", "American Samoa", "Andorra", "Angola",
      "Anguilla", "Antarctica", "Antigua And Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria",
      "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin",
      "Bermuda", "Bhutan", "Bolivia, Plurinational State of", "Bonaire, Sint Eustatius and Saba", "Bosnia and Herzegovina",
      "Botswana", "Bouvet Island", "Brazil",
      "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia",
      "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China",
      "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo",
      "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba",
      "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador", "Egypt",
      "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)",
      "Faroe Islands", "Fiji", "Finland", "France", "French Guiana", "French Polynesia",
      "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece",
      "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea",
      "Guinea-Bissau", "Guyana", "Haiti", "Heard Island and McDonald Islands", "Holy See (Vatican City State)",
      "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran, Islamic Republic of", "Iraq",
      "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan", "Jersey", "Jordan", "Kazakhstan", "Kenya",
      "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan",
      "Lao People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya",
      "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia, The Former Yugoslav Republic Of",
      "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique",
      "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of",
      "Monaco", "Mongolia", "Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru",
      "Nepal", "Netherlands", "New Caledonia", "New Zealand", "Nicaragua", "Niger",
      "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau",
      "Palestinian Territory, Occupied", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines",
      "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation",
      "Rwanda", "Saint Barthelemy", "Saint Helena, Ascension and Tristan da Cunha", "Saint Kitts and Nevis", "Saint Lucia",
      "Saint Martin (French Part)", "Saint Pierre and Miquelon", "Saint Vincent and the Grenadines", "Samoa", "San Marino",
      "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone", "Singapore",
      "Sint Maarten (Dutch Part)", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa",
      "South Georgia and the South Sandwich Islands", "South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
      "Svalbard and Jan Mayen", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic",
      "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Timor-Leste",
      "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan",
      "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom",
      "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu",
      "Venezuela, Bolivarian Republic of", "Viet Nam", "Virgin Islands, British", "Virgin Islands, U.S.",
      "Wallis and Futuna", "Western Sahara", "Yemen", "Zambia", "Zimbabwe"
    ];

    return {
      restrict: 'E',
      template: '<select><option>' + countries.join('</option><option>') + '</option></select>',
      replace: true,
      link: function (scope, elem, attrs) {
        if (!!attrs.ngModel) {
          var assignCountry = $parse(attrs.ngModel);

          elem.bind('change', function (e) {
            assignCountry(elem.val());
          });

          scope.$watch(attrs.ngModel, function (country) {
            elem.val(country);
          });
        }
      }
    };
}])
.directive('backImg', function(){
    return function(scope, element, attrs){
        var url = attrs.backImg;
        element.css({
            'background-image': 'url(' + url +')',
            'background-size' : 'cover'
        });
    };
});
var mod;

mod = angular.module('infinite-scroll', []);

mod.value('THROTTLE_MILLISECONDS', null);

mod.directive('infiniteScroll', [
  '$rootScope', '$window', '$interval', 'THROTTLE_MILLISECONDS', function($rootScope, $window, $interval, THROTTLE_MILLISECONDS) {
    return {
      scope: {
        infiniteScroll: '&',
        infiniteScrollContainer: '=',
        infiniteScrollDistance: '=',
        infiniteScrollDisabled: '=',
        infiniteScrollUseDocumentBottom: '=',
        infiniteScrollListenForEvent: '@'
      },
      link: function(scope, elem, attrs) {
        var changeContainer, checkWhenEnabled, container, handleInfiniteScrollContainer, handleInfiniteScrollDisabled, handleInfiniteScrollDistance, handleInfiniteScrollUseDocumentBottom, handler, height, immediateCheck, offsetTop, pageYOffset, scrollDistance, scrollEnabled, throttle, unregisterEventListener, useDocumentBottom, windowElement;
        windowElement = angular.element($window);
        scrollDistance = null;
        scrollEnabled = null;
        checkWhenEnabled = null;
        container = null;
        immediateCheck = true;
        useDocumentBottom = false;
        unregisterEventListener = null;
        height = function(elem) {
          elem = elem[0] || elem;
          if (isNaN(elem.offsetHeight)) {
            return elem.document.documentElement.clientHeight;
          } else {
            return elem.offsetHeight;
          }
        };
        offsetTop = function(elem) {
          if (!elem[0].getBoundingClientRect || elem.css('none')) {
            return;
          }
          return elem[0].getBoundingClientRect().top + pageYOffset(elem);
        };
        pageYOffset = function(elem) {
          elem = elem[0] || elem;
          if (isNaN(window.pageYOffset)) {
            return elem.document.documentElement.scrollTop;
          } else {
            return elem.ownerDocument.defaultView.pageYOffset;
          }
        };
        handler = function() {
          var containerBottom, containerTopOffset, elementBottom, remaining, shouldScroll;
          if (container === windowElement) {
            containerBottom = height(container) + pageYOffset(container[0].document.documentElement);
            elementBottom = offsetTop(elem) + height(elem);
          } else {
            containerBottom = height(container);
            containerTopOffset = 0;
            if (offsetTop(container) !== void 0) {
              containerTopOffset = offsetTop(container);
            }
            elementBottom = offsetTop(elem) - containerTopOffset + height(elem);
          }
          if (useDocumentBottom) {
            elementBottom = height((elem[0].ownerDocument || elem[0].document).documentElement);
          }
          remaining = elementBottom - containerBottom;
          shouldScroll = remaining <= height(container) * scrollDistance + 1;
          if (shouldScroll) {
            checkWhenEnabled = true;
            if (scrollEnabled) {
              if (scope.$$phase || $rootScope.$$phase) {
                return scope.infiniteScroll();
              } else {
                return scope.$apply(scope.infiniteScroll);
              }
            }
          } else {
            return checkWhenEnabled = false;
          }
        };
        throttle = function(func, wait) {
          var later, previous, timeout;
          timeout = null;
          previous = 0;
          later = function() {
            var context;
            previous = new Date().getTime();
            $interval.cancel(timeout);
            timeout = null;
            func.call();
            return context = null;
          };
          return function() {
            var now, remaining;
            now = new Date().getTime();
            remaining = wait - (now - previous);
            if (remaining <= 0) {
              clearTimeout(timeout);
              $interval.cancel(timeout);
              timeout = null;
              previous = now;
              return func.call();
            } else {
              if (!timeout) {
                return timeout = $interval(later, remaining, 1);
              }
            }
          };
        };
        if (THROTTLE_MILLISECONDS != null) {
          handler = throttle(handler, THROTTLE_MILLISECONDS);
        }
        scope.$on('$destroy', function() {
          container.unbind('scroll', handler);
          if (unregisterEventListener != null) {
            unregisterEventListener();
            return unregisterEventListener = null;
          }
        });
        handleInfiniteScrollDistance = function(v) {
          return scrollDistance = parseFloat(v) || 0;
        };
        scope.$watch('infiniteScrollDistance', handleInfiniteScrollDistance);
        handleInfiniteScrollDistance(scope.infiniteScrollDistance);
        handleInfiniteScrollDisabled = function(v) {
          scrollEnabled = !v;
          if (scrollEnabled && checkWhenEnabled) {
            checkWhenEnabled = false;
            return handler();
          }
        };
        scope.$watch('infiniteScrollDisabled', handleInfiniteScrollDisabled);
        handleInfiniteScrollDisabled(scope.infiniteScrollDisabled);
        handleInfiniteScrollUseDocumentBottom = function(v) {
          return useDocumentBottom = v;
        };
        scope.$watch('infiniteScrollUseDocumentBottom', handleInfiniteScrollUseDocumentBottom);
        handleInfiniteScrollUseDocumentBottom(scope.infiniteScrollUseDocumentBottom);
        changeContainer = function(newContainer) {
          if (container != null) {
            container.unbind('scroll', handler);
          }
          container = newContainer;
          if (newContainer != null) {
            return container.bind('scroll', handler);
          }
        };
        changeContainer(windowElement);
        if (scope.infiniteScrollListenForEvent) {
          unregisterEventListener = $rootScope.$on(scope.infiniteScrollListenForEvent, handler);
        }
        handleInfiniteScrollContainer = function(newContainer) {
          if ((newContainer == null) || newContainer.length === 0) {
            return;
          }
          if (newContainer instanceof HTMLElement) {
            newContainer = angular.element(newContainer);
          } else if (typeof newContainer.append === 'function') {
            newContainer = angular.element(newContainer[newContainer.length - 1]);
          } else if (typeof newContainer === 'string') {
            newContainer = angular.element(document.querySelector(newContainer));
          }
          if (newContainer != null) {
            return changeContainer(newContainer);
          } else {
            throw new Exception("invalid infinite-scroll-container attribute.");
          }
        };
        scope.$watch('infiniteScrollContainer', handleInfiniteScrollContainer);
        handleInfiniteScrollContainer(scope.infiniteScrollContainer || []);
        if (attrs.infiniteScrollParent != null) {
          changeContainer(angular.element(elem.parent()));
        }
        if (attrs.infiniteScrollImmediateCheck != null) {
          immediateCheck = scope.$eval(attrs.infiniteScrollImmediateCheck);
        }
        return $interval((function() {
          if (immediateCheck) {
            return handler();
          }
        }), 0, 1);
      }
    };
  }
]).directive('usernameAvailable', function($timeout, $q, $http, baseUrl) {
  return {
    restrict: 'AE',
    require: 'ngModel',
    link: function(scope, elm, attr, model) { 
      model.$asyncValidators.usernameExists = function() { 
        //here you should access the backend, to check if username exists
        //and return a promise
        // var defer = $q.defer();
        // $timeout(function(){
        //   model.$setValidity('usernameExists', true); 
        //   defer.resolve;
        // }, 1000);
        // return defer.promise;
        
        var username = model.$viewValue;
        return $http.post(baseUrl + '/account/username/check', {username: username}).then(function(res){+
          $timeout(function(){
            console.log(res);
            model.$setValidity('usernameExists', !!res.data.available); 
          }, 100);
        }); 
        
        
      };
    }
  } 
}).directive('emailAvailable', function($timeout, $q, $http, baseUrl) {
  return {
    restrict: 'AE',
    require: 'ngModel',
    link: function(scope, elm, attr, model) { 
      model.$asyncValidators.emailExists = function() { 
        //here you should access the backend, to check if email exists
        //and return a promise
        // var defer = $q.defer();
        // $timeout(function(){
        //   model.$setValidity('emailExists', true); 
        //   defer.resolve;
        // }, 1000);
        // return defer.promise;
        
        var email = model.$viewValue;
        return $http.post(baseUrl + '/account/email/check', {email: email}).then(function(res){+
          $timeout(function(){
            console.log(res);
            model.$setValidity('emailExists', !!res.data.available); 
          }, 100);
        }); 
        
        
      };
    }
  } 
});
