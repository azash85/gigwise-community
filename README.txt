This project is built using angularjs and bootstrap 3 using bower to install dependencies and gulp to minify and concatinate.

Once you pull the project for the first time you need npm (node project manager) and bower installed. Once that is setup you need to do
a "npm install" in the project-folder and a "bower install"

Once that is setup you might need to change the index.html <base href="/community/" /> to <base href="/" /> (community is for the live
version to work). Also might have to change .htaccess  RewriteRule ^ /community/index.html to  RewriteRule ^ /index.html (community is also
here meant for the live version). In app.js line 4 and 5 you can change between live and test API. 

Live API: http://api.gigwise.com
Test API: http://api.getblow.co 

God speed!