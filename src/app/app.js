angular.module('mainApp', ['ngRoute', 'ui.bootstrap', 'restangular', 'ngFileUpload', 'ngCookies', 'textAngular', 'as.sortable', 'ngImgCrop', 'ngAnimate', 'toastr', 'angular-medium-editor',
'mwl.confirm', 'infinite-scroll', 'sticky', 'satellizer']).

constant( 'baseUrl', 'http://api.gigwise.com').
//constant( 'baseUrl', 'http://api.getblow.co').

config(function ($routeProvider, $locationProvider, baseUrl, $httpProvider, RestangularProvider, $provide, toastrConfig, $tooltipProvider, $authProvider) {
    
    RestangularProvider.setBaseUrl(baseUrl);
    
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common['X-Requested-With'];

    $routeProvider
    //.when('/', { 
    //    controller: 'homeCtrl', 
    //    templateUrl: 'src/app/views/home.html'
    //})
    .when('/latest', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'latest'
    })
    .when('/featured', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'featured'
    })
    .when('/top-users', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'top_users'
    })
    .when('/top-posts', { 
        controller: 'homeCtrl', 
        templateUrl: 'src/app/views/home.html',
        activetab: 'top_posts'
    })
     
    .when('/create-feature', { 
        controller: 'create_featureCtrl', 
        templateUrl: 'src/app/views/create_feature.html' 
    })
    .when('/edit-feature/:id', { 
        controller: 'create_featureCtrl', 
        templateUrl: 'src/app/views/create_feature.html' 
    })
    .when('/create-slideshow', { 
        controller: 'create_slideshowCtrl', 
        templateUrl: 'src/app/views/create_slideshow.html' 
    })
    .when('/edit-slideshow/:id', { 
        controller: 'create_slideshowCtrl', 
        templateUrl: 'src/app/views/create_slideshow.html' 
    })
    .when('/create-gigphotos', { 
        controller: 'create_gigphotosCtrl', 
        templateUrl: 'src/app/views/create_gigphotos.html' 
    })
    .when('/edit-gigphotos/:id', { 
        controller: 'create_gigphotosCtrl', 
        templateUrl: 'src/app/views/create_gigphotos.html' 
    })
    .when('/create-review', { 
        controller: 'create_reviewCtrl', 
        templateUrl: 'src/app/views/create_review.html' 
    })
    .when('/edit-review/:id', { 
        controller: 'create_reviewCtrl', 
        templateUrl: 'src/app/views/create_review.html' 
    })
    .when('/create-musicnews', { 
        controller: 'create_musicnewsCtrl', 
        templateUrl: 'src/app/views/create_musicnews.html' 
    })
    .when('/edit-musicnews/:id', { 
        controller: 'create_musicnewsCtrl', 
        templateUrl: 'src/app/views/create_musicnews.html' 
    })
    .when('/profile', { 
        controller: 'profileCtrl', 
        templateUrl: 'src/app/views/profile.html' 
    })
    .when('/settings', { 
        controller: 'profile_settingsCtrl', 
        templateUrl: 'src/app/views/profile_settings.html',
        activetab: 'settings'
    })
    .when('/my-articles', { 
        controller: 'profile_settingsCtrl', 
        templateUrl: 'src/app/views/profile_settings.html',
        activetab: 'my-articles'
    })
    .when('/article/:id/:title', { 
        controller: 'view_articleCtrl', 
        templateUrl: 'src/app/views/view_article.html' 
    })
    .otherwise({ redirectTo: '/latest' });
    
    //textAngular create custom button
    $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions){
        // $delegate is the taOptions we are decorating
        // register the tool with textAngular
        taRegisterTool('colourRed', {
            iconclass: "fa fa-image",
            action: function(){
                this.$editor().wrapSelection('forecolor', 'red');
            }
        });
        // add the button to the default toolbar definition
        //taOptions.toolbar[1].push('colourRed');
        return taOptions;
    }]);
    
   $provide.decorator('taOptions', ['taRegisterTool', '$delegate', '$modal', 'taToolFunctions', 'toastr', function (taRegisterTool, taOptions, $modal, taToolFunctions, toastr) {
        taRegisterTool('myUploadImage', {
            iconclass: "fa fa-image",
            tooltiptext: "Insert Image",
            action: function (deferred) {
                $modal.open({
                    templateUrl: './src/app/views/embed_image_modal.html',
                    controller: function($scope, $modalInstance, Upload, baseUrl, $cookies) {
                        $scope.image = '';
                        $scope.progress = 0;
                        $scope.test = function(file){
                            console.log(file);
                        };
                        $scope.upload = function(file, validate){
                            if(validate.length < 1){
                                Upload.upload({
                                    url: baseUrl+'/article/photo/upload',
                                    fields: {'access_token': $cookies.get('access_token')},
                                    file: file,
                                    method: 'POST'
                                }).progress(function (evt) {
                                    $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
                                }).success(function (response) {
                                    $scope.progress = 0;
                                    $scope.image = response.photo_url;
                                });
                            }
                            else{
                                toastr.error("Acceptable images are: JPG, TIF, PNG, and GIF.", "File Error");
                            }
                        };
                    
                        $scope.insert = function(){
                            $modalInstance.close($scope.image);
                        };
                    }
                }).result.then(
                    function (result) {
                        var concat = '<img src="'+result+'" style="max-width:650px">';
                        document.execCommand('insertHTML', false, concat);
                        //document.execCommand('insertImage', true, result);
                        deferred.resolve();
                    },
                    function () {
                        deferred.resolve();
                    }
                );
                return false;
            },
            onElementSelect: {
            	element: 'img',
            	action: taToolFunctions.imgOnSelectAction
            }
        });
        //taOptions.toolbar[1].push('myUploadImage');
        return taOptions;
    }]);
    
    angular.extend(toastrConfig, {
        autoDismiss: false,
        containerId: 'toast-container',
        maxOpened: 0,    
        newestOnTop: true,
        positionClass: 'toast-bottom-right',
        preventDuplicates: false,
        preventOpenDuplicates: false,
        target: 'body'
    })
    
    $authProvider.facebook({
      url: '/community/auth/facebook',
      clientId: '468461729999988',
      scope: ['email', 'public_profile', 'user_friends'],
      redirectUri: window.location.origin + '/community/'
    });
  
});