angular.module('mainApp')
.controller('mainCtrl', function($scope, $rootScope, $cookies, $modal, $location, Restangular, taApplyCustomRenderers, $q, Upload, baseUrl, toastr, $window){
	var save_featureAPI = Restangular.all('/article/save');
	var save_reviewAPI = Restangular.all('/review/save');
	var save_slideshowAPI = Restangular.all('/slideshow/save');
	var save_gigphotosAPI = Restangular.all('/gigphotos/save');
	var save_musicnewsAPI = Restangular.all('/musicnews/save');
	var save_profileInfoAPI = Restangular.all('/account/info/save');
	var save_passwordAPI = Restangular.all('/account/password/change');
	var publish_articleAPI = Restangular.all('/article/publish');
	var unpublish_articleAPI = Restangular.all('/article/unpublish');
	var username_takenAPI = Restangular.all('/account/username/check');
	var save_usernameAPI = Restangular.all('/account/username/set');
	var check_tokenAPI = Restangular.all('/account/istokenvalid');
	var signupAPI = Restangular.all('web/signup');
	var get_tagsAPI = Restangular.all('/article/tags/get');
	
	//get_tagsAPI.post({access_token: $cookies.get('access_token')}).then(function(result){
	//	console.log(result.tags);
	//})
	$rootScope.profile_photo = $cookies.get('profile_photo');
	$rootScope.access_token = $cookies.get('access_token');
	
	//Confirm leaving article-pages
	$scope.$on('$locationChangeStart', function(event, next, current) {
        if($rootScope.confirm_location_change){
            var answer = confirm("Are you sure you want to leave this page? \nAny unsaved changes will be lost.");
            if (!answer) {
                event.preventDefault();
            } 
        }
    });

	//Check if access_token is valid on page-change. Else redirect to homepage
	$scope.$on('$routeChangeStart', function(next, current) {
		if($cookies.get('access_token')){
			check_tokenAPI.post({access_token: $cookies.get('access_token')}).then(function(response){
				//access token valid
			},function(error){
				$rootScope.loading = false;
				$cookies.remove('access_token');
				$location.path('/');
			});
		}
		else{
			if(current.activetab != 'latest' && current.activetab != 'top_posts' && current.activetab != 'top_users'){
				$location.path('/');
			}
		}
		
		//handles if you publish from create-articles to route to the published tab under my-articles
		if(current.activetab != 'my-articles'){
			$rootScope.publish_route = false;
		}
	});
	
	$scope.log = function(log){
		console.log(log);
	}
	$scope.validateImage = function(file, photos){
		if(file.length > 0){
			toastr.error("Acceptable images are: JPG, TIF, PNG, and GIF.", "File Error");
		}
	}
	$scope.signUp = function(signup){
		$rootScope.loading = true;
		var params = {first_name: signup.firstname,last_name: signup.lastname, email: signup.email, username: signup.username, password: signup.password};
        signupAPI.post(params).then(function(response){
			$scope.openConfirmSignupModal(signup.email);
        }, function(error){
            console.log(error);
        }).finally(function(){
			$rootScope.loading = false;
		});
	}
	$scope.logout = function(){
		$rootScope.access_token = '';
		$cookies.remove('access_token');
		$location.path('/');
	}
	$scope.goViewArticle = function(id, title){
		$location.path('/article/'+id +'/'+title);
	}
	$scope.openCreatePostModal = function(data){
		var templateUrl = './src/app/views/create_post_modal.html';
		var size = 'md';
		var windowClass = 'create-post-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openPublishtModal = function(data){
		var templateUrl = './src/app/views/publish_modal.html';
		var size = 'md';
		var windowClass = 'publish-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openCropModal = function(data){
		var templateUrl = './src/app/views/crop_modal.html';
		var size = 'md';
		var windowClass = 'crop-modal';
		var backdrop = 'static';
		var keyboard = false;
		$modal.open({
			animation: true,
			backdrop: backdrop,
			keyboard: keyboard,
			templateUrl: templateUrl,
			controller: 'ModalCtrl',
			size: size,
			windowClass: windowClass,
			resolve: {
				data: function () {
					return data;
				}
			}
    	}).result.then(function(result) {
			if(result){
				data.photo = result.photo_cropped;
			}
		});
	}
	$scope.openLoginModal = function(data){
		var templateUrl = './src/app/views/login_modal.html';
		var size = 'md';
		var windowClass = 'login-modal';
		openModal(size, templateUrl, windowClass, data);
	}
    $scope.openPreviewModal = function(data){
		var templateUrl = './src/app/views/preview_article_modal.html';
		var size = 'md';
		var windowClass = 'preview-article-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openPreviewSlideshowModal = function(data){
		var templateUrl = './src/app/views/preview_slideshow_modal.html';
		var size = 'lg';
		var windowClass = 'preview-slideshow-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openImageLoadingModal = function(data){
		var templateUrl = './src/app/views/image_loading_modal.html';
		var size = 'md';
		var windowClass = 'preview-article-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	$scope.openConfirmSignupModal = function(data){
		var templateUrl = './src/app/views/confirm_signup_modal.html';
		var size = 'md';
		var windowClass = 'confirm-signup-modal';
		openModal(size, templateUrl, windowClass, data);
	}
	
	$scope.redirectNewTab = function(link){
        $window.open(link, '_blank');
    };
	
	$scope.saveUsername = function(access_token, profile){
		if(profile.username == profile.original_username){
			toastr.info('Username hasnt changed', 'Username');
		}
		else{
			var params = {username: profile.username};
			username_takenAPI.post(params).then(function(response){
				params = {access_token: access_token, username: profile.username};
				save_usernameAPI.post(params).then(function(response){
					profile.original_username = profile.username;
					toastr.success('Username saved!', 'Username');
				}, function(error){
					console.log(error);
					toastr.error(error.data.message, 'Username');
				});
			}, function(error){
				console.log(error);
				toastr.error(error.data.message, 'Username');
			});
		}
	}
	
	$scope.publishArticle = function(access_token, article, publish_from_edit){
		console.log(article);
		var params = {access_token: access_token, article_id: article.id}
		publish_articleAPI.post(params).then(function(response){
			article.is_draft = 0;
			article.link_to_article = response.article.link_to_article;
			article.facebook_link = response.article.facebook_link;
			$rootScope.publish_route = true;
			if(!publish_from_edit){
				$scope.openPublishtModal(article);
			}
			toastr.success('Your article has been published!', 'Article');
		}, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Article');
        }).finally(function(){
			
		});
	}
	
	$scope.unpublishArticle = function(access_token, article){
		var params = {access_token: access_token, article_id: article.id}
		unpublish_articleAPI.post(params).then(function(response){
			article.is_draft = 1;
			toastr.success('Your article has been unpublished!', 'Article');
		}, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Article');
        }).finally(function(){
			
		});
	}
	
	$scope.changePassword = function(access_token, old_password, new_password){
		var params = {access_token: access_token, old_password: old_password, new_password: new_password}
		save_passwordAPI.post(params).then(function(response){
			toastr.success('Password-change successfull!', 'Settings');
        }, function(error){
            console.log(error);
			toastr.error(error.data.message, 'Settings');
        }).finally(function(){
			
		});
	}
	
	$scope.saveProfile = function(access_token, profile){
		$rootScope.loading = true;
		if(!profile.photo_url){
			profile.photo_url = '';
		}
		var photo = [profile.photo_url];
		var params = {access_token: access_token, email: profile.email, first_name: profile.first_name, last_name: profile.last_name, country: profile.country, gender: profile.gender,
		bio: profile.bio, website: profile.website};
        save_profileInfoAPI.post(params).then(function(response){
			if(photo[0].length > 3000){ //if the photo is a file
				photo[0] = Upload.dataUrltoBlob(photo[0]);
				var url = baseUrl + '/account/photo/save';
        		var promise_a = uploadMultilpleImages($cookies.get('access_token'), '', url, photo);
				$q.all([promise_a]).then(function(response){
					profile.photo_url = response[0][0].data.photo_url;
					$cookies.put('profile_photo', profile.photo_url);
					$rootScope.profile_photo = $cookies.get('profile_photo');
					$rootScope.loading = false;
					toastr.success('Your profile has been saved!', 'Settings');
				});
			}
			else{
				$rootScope.loading = false;
				toastr.success('Your profile has been saved!', 'Settings');
			}	
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
		
	//Save Feature API-call
	$scope.saveFeature = function(access_token, feature, publish){
		//var uncompiled_long_desc = feature.long_desc;
		//feature.long_desc = taApplyCustomRenderers(feature.long_desc);
		
		var data = {}
		data.publish = publish;
		data.article = feature;
		if(feature.photo && feature.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [feature.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(feature.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(feature.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: feature.title, short_desc: feature.short_desc, long_desc: feature.long_desc, article_id: feature.id, photographer: feature.photographer, tags: {}};
        save_featureAPI.post(params).then(function(response){
			feature.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(feature.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, feature.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					feature.photo = '';
					feature.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, feature, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Review API-call
	$scope.saveReview = function(access_token, review, publish){
		//review.long_desc = taApplyCustomRenderers(review.long_desc);
		var data = {}
		data.publish = publish;
		data.article = review;
		if(review.photo && review.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [review.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(review.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(review.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: review.title, short_desc: review.short_desc, long_desc: review.long_desc, article_id: review.id, photographer: review.photographer, tags: {}};
        save_reviewAPI.post(params).then(function(response){
			review.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(review.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, review.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					review.photo = '';
					review.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}	
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, review, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Musicnews API-call
	$scope.saveMusicnews = function(access_token, musicnews, publish){
		//musicnews.long_desc = taApplyCustomRenderers(musicnews.long_desc);
		var data = {}
		data.publish = publish;
		data.article = musicnews;
		if(musicnews.photo && musicnews.photo.size){ //if the photo is an uncropped file
			data.files_to_send = [musicnews.photo];
			$scope.openImageLoadingModal(data);	
		}
		else if(musicnews.photo_url){//if the photo is an url
			$rootScope.loading = true;
		}
		else{ //the photo has been cropped
			data.files_to_send = [Upload.dataUrltoBlob(musicnews.photo)];
			$scope.openImageLoadingModal(data);	
		}
		var params = {access_token: access_token, title: musicnews.title, short_desc: musicnews.short_desc, long_desc: musicnews.long_desc, article_id: musicnews.id, photographer: musicnews.photographer, tags: {}};
        save_musicnewsAPI.post(params).then(function(response){
			musicnews.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Feature has been saved!', 'Feature');
			if(musicnews.photo){ //if the photo is a file
				var url = baseUrl + '/article/coverphoto/update';
        		var promise_a = uploadMultilpleImages(access_token, musicnews.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					musicnews.photo = '';
					musicnews.photo_url = response[0][0].data.photo_url;
					toastr.success('Your Feature-photo has been saved!', 'Feature');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, musicnews, false);
			}		
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Gigphotos
	$scope.saveGigphotos = function(access_token, gigphotos, publish){
		var data = {}; //data for modal
		data.publish = publish;
		data.article = gigphotos;
		data.files_to_send = [];

		angular.forEach(gigphotos.photos, function(photo, index){
			if(photo.size){ // if the photo is a file
            	data.files_to_send.push(photo);
			}
			else{
				gigphotos.existing_images.push(photo);
			}
        });
		
		if(gigphotos.photo){ //if the photo is file
			if(gigphotos.photo.size){ //if the photo is an uncropped file
				gigphotos.photo.is_cover_photo = true;
			}
			else{ //if the photo is a cropped fle
				gigphotos.photo = Upload.dataUrltoBlob(gigphotos.photo);
				gigphotos.photo.is_cover_photo = true;
			}
			data.files_to_send.push(gigphotos.photo);	
			$scope.openImageLoadingModal(data);	
		}
		else if(data.files_to_send.length < 1){//if there are no files to send
			$rootScope.loading = true;
		}
		else{
			$scope.openImageLoadingModal(data);	// if the cover-photo hasnt changed
		}
		var params = {access_token: access_token, title: gigphotos.title, short_desc: gigphotos.short_desc, long_desc: gigphotos.long_desc, article_id: gigphotos.id, tags: {}, 
		deleted_images: gigphotos.deleted_images, existing_images: gigphotos.existing_images, photographer: gigphotos.photographer};
        save_gigphotosAPI.post(params).then(function(response){
			gigphotos.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Gig has been saved!', 'Gigphotos');
			if(data.files_to_send.length > 0){
				var url = baseUrl + '/gigphotos/photo/upload';
        		var promise_a = uploadMultilpleImages(access_token, gigphotos.id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					toastr.success('Your Gig-photos have been saved!', 'Gigphotos');
				}, function (error) {
					data.file_sent = true;
					console.log('herpaderp');
					toastr.error('Some of you Gig-photos didnt upload.', 'Gigphotos');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, gigphotos, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Save Slideshow API-call
	$scope.saveSlideshow = function(access_token, slideshow, publish){
		var data = {}; //data for modal
		data.publish = publish;
		data.article = slideshow;
		data.files_to_send = [];
		//data.files_to_send = slideshow.photos.slice();
		
		angular.forEach(slideshow.photos, function(photo, index){
			if(photo.size){ // if the photo is a file
            	data.files_to_send.push(photo);
			}
			else{
				slideshow.existing_images.push(photo);
			}
        });
		
		if(slideshow.photo){ //if the photo is file
			if(slideshow.photo.size){ //if the photo is an uncropped file
				slideshow.photo.is_cover_photo = true;
			}
			else{ //if the photo is a cropped fle
				slideshow.photo = Upload.dataUrltoBlob(slideshow.photo);
				slideshow.photo.is_cover_photo = true;
			}
			data.files_to_send.push(slideshow.photo);	
			$scope.openImageLoadingModal(data);	
		}
		else if(data.files_to_send.length < 1){//if there are no files to send
			$rootScope.loading = true;
		}
		else{
			$scope.openImageLoadingModal(data);	// if the cover-photo hasnt changed
		}
		var params = {access_token: access_token, title: slideshow.title, short_desc: slideshow.short_desc, long_desc: slideshow.long_desc, article_id: slideshow.id, tags: {}, 
		deleted_images: slideshow.deleted_images, existing_images: slideshow.existing_images, photographer: slideshow.photographer};
        save_slideshowAPI.post(params).then(function(response){
			slideshow.id = response.article_id;
			$rootScope.loading = false;
			toastr.success('Your Slideshow has been saved!', 'Slideshow');
			if(data.files_to_send.length > 0){
				var url = baseUrl + '/slideshow/photo/upload';
        		var promise_a = uploadMultilpleImages(access_token, response.article_id, url, data.files_to_send);
				$q.all([promise_a]).then(function(response){
					data.file_sent = true;
					toastr.success('Your Slideshow-photos have been saved!', 'Slideshow');
				}, function (error) {
					data.file_sent = true;
					toastr.error('Some of you Slideshow-photos didnt upload.', 'Slideshow');
				});
			}
			else if(publish){ //if you want to publish the feature right away and you dont have to save the photo
				$scope.publishArticle(access_token, slideshow, false);
			}
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	function openModal(size, templateUrl, windowClass, data) {
		var backdrop = 'true';
		var keyboard = true;
		if(!data){
			data = {};
		}
		data.templateUrl = templateUrl;
		if(templateUrl == './src/app/views/image_loading_modal.html'){
			backdrop = 'static';
			keyboard = false;
		}
		$modal.open({
			animation: true,
			backdrop: backdrop,
			keyboard: keyboard,
			templateUrl: templateUrl,
			controller: 'ModalCtrl',
			size: size,
			windowClass: windowClass,
			resolve: {
				data: function () {
					return data;
				}
			}
    	});
	}
	//For handling multiple uploads
    function uploadMultilpleImages(token, article_id, photos_url, files){
        var deferred = $q.defer();
        var queue = [];

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var promise = uploadImage(token, article_id, photos_url, files[i]);
                queue.push(promise);
            }
        }

        $q.all(queue).then(
            function (data) {
                deferred.resolve(data)
            },
            function (err) {
                deferred.reject(err)
            }
        )

        return deferred.promise
    }
    //Called from uploads-function
    function uploadImage(token, article_id, url, file) {
		var fields = {access_token: token, article_id: article_id};
		if(url == (baseUrl + '/slideshow/photo/upload')){
			fields = {access_token: token, article_id: article_id, order_id: file.order_id, credit: file.credit, title: file.title, alt: file.alt, is_cover_photo: file.is_cover_photo};
		}
		if(url == (baseUrl + '/gigphotos/photo/upload')){
			fields = {access_token: token, article_id: article_id, order_id: file.order_id, is_cover_photo: file.is_cover_photo};
		}
        var promise = Upload.upload({
            url: url,
            headers: {'Content-Type': "multipart/form-data"},
            method: 'POST',
            fields: fields,
            file: file
        }).progress(function (evt) {
            var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            file.progress = progressPercentage;
        }).success(function (data, status, headers, config) {
            file.result = 'file ' + config.file.name + ' uploaded. Response: ' + data;
            //$scope.test_picture = data.photo_url;
        }).catch(function(error){
			console.log(error);
			file.error = error.data.error;
		});
        return promise;
    };
})
.controller('ModalCtrl', function ($scope, $modalInstance, data, Restangular, $rootScope, $location, Upload, $cookies, $route, toastr, $auth) {
	var loginAPI = Restangular.all('web/login');
	var forgotpasswordAPI = Restangular.all('web/forgotemail');
	var facebookSigninAPI = Restangular.all('web/fblogin');
	$scope.access_token = $cookies.get('access_token');
	
	//Login-modal code
	$scope.signIn = function(email, password){
		$scope.login_error_msg = '';
		var params = {email: email, password: password};
        loginAPI.post(params).then(function(response){
			$cookies.put('access_token', response.access_token);
			$rootScope.access_token = response.access_token;
			$cookies.put('profile_photo', response.profile_photo);
			$rootScope.profile_photo = $cookies.get('profile_photo');
			$modalInstance.close();
			$location.path('/my-articles');
        }, function(error){
			$scope.login_error_msg = error.data.message;
            console.log(error);
        });
	}
	
	$scope.facebookSignup = function(provider) {
      	$auth.authenticate(provider).then(function(response) {
			$modalInstance.close();
			$rootScope.loading = true;
			var params = {code: response.config.data.code, redirect_uri: response.config.data.redirectUri};
			facebookSigninAPI.post(params).then(function(response){
				$rootScope.loading = false;
				console.log(response);
				$cookies.put('access_token', response.access_token);
				$rootScope.access_token = response.access_token;
				$cookies.put('profile_photo', response.profile_photo);
				$rootScope.profile_photo = $cookies.get('profile_photo');
				$location.path('/my-articles');
			}, function(error){
				$rootScope.loading = false;
				$scope.login_error_msg = error.data.message;
				console.log(error);
			});
    	}).catch(function(response) {
			$rootScope.loading = false;
          	console.log(response);
        });
    };
	
	$scope.resetPassword = function(email){
		$modalInstance.close();
		$rootScope.loading = true;
		var params = {email: email};
        forgotpasswordAPI.post(params).then(function(response){
			$rootScope.loading = false;
			alert("An email has been sent to "+email+" with details on how to reset your password");
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	
	//Create-Post-modal code
	$scope.forwardFeature = function(){
		$modalInstance.close();
		$location.path('/create-feature');
	}
	$scope.forwardReview = function(){
		$modalInstance.close();
		$location.path('/create-review');
	}
	$scope.forwardGigPhotos = function(){
		$modalInstance.close();
		$location.path('/create-gigphotos');
	}
	$scope.forwardSlideshow = function(){
		$modalInstance.close();
		$location.path('/create-slideshow');
	}
	$scope.forwardMusicnews = function(){
		$modalInstance.close();
		$location.path('/create-musicnews');
	}
	
	//Preview-Article-modal code
	$scope.article = _.cloneDeep(data);
	$scope.article.photo_cropped = '';
	
	//loading modal code
	$scope.modal_data = data;
	
	//slideshow modal
	if(data.templateUrl == './src/app/views/preview_slideshow_modal.html'){

		if($scope.article.photos.length > 0 && $scope.article.cover_photo){
			if($scope.article.photos[0]['file_name']){
				$scope.article.photos.unshift({file_name: $scope.article.cover_photo});
			}
			if($scope.article.photos[0]['size']){
				$scope.article.photos.unshift($scope.article.cover_photo);
			}
		}
		else{
			toastr.info('Please select at least one photo and cover-photo', 'Images');
		}
	}
	
	
	
	$scope.ok = function () {
		if($scope.modal_data.file_sent){
			if($scope.modal_data.article.post_type_id){ //if the article isnt new
				$modalInstance.close();
				$route.reload();
			}
			else{//the article is new
				$rootScope.confirm_location_change = false;
				$location.path('/edit-'+$scope.modal_data.article.type+'/'+$scope.modal_data.article.id);
				$modalInstance.close();
			} 
			
		}
	};
	$scope.ok_signup = function () {
		$modalInstance.close();
		$route.reload();
	};
	$scope.cancel = function () {
		$modalInstance.dismiss();
		$route.reload();
	};
	$scope.ok_modal = function () {
		$modalInstance.close($scope.article);
	};
	$scope.cancel_modal = function () {
		$modalInstance.close();
	};
	$scope.ok_publish_modal = function(){
		$rootScope.confirm_location_change = false;
		$modalInstance.close();
		$location.path('/my-articles');	
	}
	
	
	
	if(data.templateUrl == './src/app/views/test_modal.html'){
		$scope.image = 'img/default.png';
	
		$scope.progress = 0;
		$scope.files = [];
	}
	
	$scope.upload = function(){
		Upload.upload({
			url: 'api/upload',
			fields: {'dir': 'img/uploads/'},
			file: $scope.files[0],
			method: 'POST'
		}).progress(function (evt) {
			$scope.progress = parseInt(100.0 * evt.loaded / evt.total);
		}).success(function (data) {
			$scope.progress = 0;
			$scope.image = data.dir+data.filename;
		});
	};

	$scope.insert = function(){
		$modalInstance.close($scope.image);
	};
	
	$scope.validateEmail = function(email) {
		var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
	
});