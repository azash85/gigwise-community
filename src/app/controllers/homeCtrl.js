angular.module('mainApp')
.controller('homeCtrl', function ($scope, $location, $rootScope, baseUrl, $modal, $cookies, Restangular, $window, $route) {
	var load_homepagearticlesAPI = Restangular.all('/public/articles/homepage');
	var top_postsAPI = Restangular.all('public/articles/topposts');
	var top_usersAPI = Restangular.all('public/articles/topusers');
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;
	
	$scope.active_tab = {latest: false, featured: false, top_users: false, top_posts: false}
	$scope.articles = [];
	$scope.pagination = {received: 0, requested: 5, maxed: true};
	
	evaluateTab($route.current.$$route.activetab);
	
	$scope.loadMore = function(page){
		getArticles(page);
	}
	$scope.redirectHome = function(path){
		$location.path(path);
	}
	$scope.loadPostsList = function(list){
		switch(list) {
			case 'Week':
				$scope.loaded_list = $scope.top_posts.top_posts_week;
				break;
			case 'Month':
				$scope.loaded_list = $scope.top_posts.top_posts_month;
				break;
			case 'Year':
				$scope.loaded_list = $scope.top_posts.top_posts_year;
				break;
		}
	}
	$scope.loadUsersList = function(list){
		switch(list) {
			case 'Week':
				$scope.loaded_list = $scope.top_users.top_users_week;
				break;
			case 'Month':
				$scope.loaded_list = $scope.top_users.top_users_month;
				break;
			case 'Year':
				$scope.loaded_list = $scope.top_users.top_users_year;
				break;
		}
	}
	$scope.forwardToArticle = function(url){
		window.open(url, '_blank');
	}
		
	function evaluateTab(tab){
		$scope.active_tab = {latest: false, featured: false, top_users: false, top_posts: false}
		switch(tab) {
			case 'latest':
				$scope.active_tab.latest = true;
				getArticles($scope.pagination);
				break;
			case 'featured':
				$scope.active_tab.featured = true;
				break;
			case 'top_users':
				$scope.active_tab.top_users = true;
				getTopUsers();
				break;
			case 'top_posts':
				$scope.active_tab.top_posts = true;
				getTopPosts();
				break;
			default:
				$scope.active_tab.latest = true;
				getArticles($scope.pagination);
				break;
		}
	}
	
	function getArticles(page){
		if(!$rootScope.loading && $scope.active_tab.latest){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			var params = {number_of_articles_received: page.received, number_of_articles_requested: page.requested};
			load_homepagearticlesAPI.post(params).then(function(response){
				//console.log(page);
				page.maxed = false;
				$rootScope.loading = false;
				for (var i=0; i<response.articles.length; i++) {
					$scope.articles.push(response.articles[i]);
				}
				page.received += response.articles.length;
				if(page.requested > response.articles.length || response.articles.length == 0){
					page.maxed = true;
				}
			},function(error){
				console.log(error);
				$rootScope.loading = false;
			}).finally(function(){
				$rootScope.loading_background = '';
			});
		}
	}
	function getTopPosts(){
		if(!$rootScope.loading && $scope.active_tab.top_posts){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			top_postsAPI.post().then(function(response){
				$scope.top_posts = response.top_posts;
				$scope.loaded_list = response.top_posts.top_posts_month;
			},function(error){
				console.log(error);
			}).finally(function(){
				$rootScope.loading = false;
				$rootScope.loading_background = '';
			});
		}
	}
	function getTopUsers(){
		if(!$rootScope.loading && $scope.active_tab.top_users){
			$rootScope.loading_background = 'transparent';
			$rootScope.loading = true;
			top_usersAPI.post().then(function(response){
				$scope.top_users = response.top_users;
				$scope.loaded_list = response.top_users.top_users_month;
			},function(error){
				console.log(error);
			}).finally(function(){
				$rootScope.loading = false;
				$rootScope.loading_background = '';
			});
		}
	}
});