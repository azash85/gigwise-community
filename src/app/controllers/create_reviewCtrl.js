angular.module('mainApp')
.controller('create_reviewCtrl', function ($scope, $location, $rootScope, baseUrl, Restangular, $q, Upload, $modal, $cookies, $routeParams) {
    var get_reviewAPI = Restangular.all('/review/load');
    $scope.review = {};
    $scope.review.timestamp = formatDateTime(new Date());
    $scope.review.type = 'review';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    //If id is set in the url get that review
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_reviewAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.review = response.article;
            var converted_date = new Date(Date.parse($scope.review.created_at));
			$scope.review.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
})