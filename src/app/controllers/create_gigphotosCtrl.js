angular.module('mainApp')
.controller('create_gigphotosCtrl', function($scope, $rootScope, $cookies, Restangular, baseUrl, $location, $routeParams, $q, Upload, toastr){
	var get_gigphotosAPI = Restangular.all('/gigphotos/load');
    
    $scope.gigphotos = {};
    $scope.gigphotos.photos = [];
    $scope.gigphotos.timestamp = formatDateTime(new Date());
    $scope.gigphotos.type = 'gigphotos';

    //confirm leaving the page
    $rootScope.confirm_location_change = true;
    
    $scope.sortableOptions = { 
        containerPositioning:'relative',
        additionalPlaceholderClass: 'col-sm-4',
        //containment: '#board',
        //restrict move across columns. move only within column.
        accept: function (sourceItemHandleScope, destSortableScope) {
            //console.log(sourceItemHandleScope.itemScope.sortableScope.$id + " " + destSortableScope.$id);
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        },
        orderChanged: function (event){
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    };
    
    $scope.multiSelectCredit = function(credit){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.credit = credit;
        });
    }
    $scope.multiSelectTitle = function(title){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.title = title;
        });
    }
    $scope.multiSelectCaption = function(caption){
        angular.forEach($scope.gigphotos.photos, function(photo){
            photo.alt = caption;
        });
    }
    
    function sortOrderIds(gigphotos_photos){
        angular.forEach(gigphotos_photos, function(photo, index){
            photo.order_id = index;
        });
        return gigphotos_photos;
    }
    
    $scope.$watch('gigphotos_photos', function (photos) {
        if($scope.gigphotos_photos){
            $scope.gigphotos.photos.push.apply($scope.gigphotos.photos, photos);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    });
    
    //$scope.$watch('gigphotos.photos', function (photos) {
    //    $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
    //});
    
    $scope.deleteImage = function(image){
        if(image.size){ //check if image is a file
            $scope.gigphotos.photos.splice(image.order_id, 1);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
        if(image.id){ //if the image has an id its a url and not a file
            $scope.gigphotos.deleted_images.push({id: image.id}); //add the deleted id to array for proper deletion at the server
            $scope.gigphotos.photos.splice(image.order_id, 1);
            $scope.gigphotos.photos = sortOrderIds($scope.gigphotos.photos);
        }
    }
    
   
    //If id is set in the url get that gigphotos
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_gigphotosAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.gigphotos = response.article;
            $scope.gigphotos.photos = response.images;
            $scope.gigphotos.deleted_images = []; //to keep track of deleted images || photos.id
            $scope.gigphotos.existing_images = []; //to keep track of the existing url's for saved images and their order || photos.id and photos.order_id
            //$rootScope.$broadcast('elastic:adjust');
            var converted_date = new Date(Date.parse($scope.gigphotos.created_at));
			$scope.gigphotos.timestamp = formatDateTime(converted_date);
        }, function(error){
            console.log(error);
        });
    }
    
    $scope.disableToastr = function(form){
        if(!form.$valid) {
            toastr.info('Please fill out all the fields and select photo','Info');
        }
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
});