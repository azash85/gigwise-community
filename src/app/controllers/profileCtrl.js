angular.module('mainApp')
.controller('profileCtrl', function ($scope, $location, $rootScope, baseUrl, $modal, Restangular, $cookies) {
	var my_articlesAPI = Restangular.all('/account/articles/get');
	var loadProfileAPI = Restangular.all('/account/info/load');
	
	loadProfile($cookies.get('access_token'));
	$scope.articles = {};
	getArticles($cookies.get('access_token'));
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;

	function getArticles(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        my_articlesAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.articles = response.articles;
        }, function(error){
            console.log(error);
        });
	}
	//returns array of specific length for mock ng-repeat
	$scope.getNumber = function(num) {
        return new Array(num);   
	}
	
	function loadProfile(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        loadProfileAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.user = response.user;
			$rootScope.profile_photo = response.user.photo_url;
        }, function(error){
            console.log(error);
        });
	}
});