angular.module('mainApp')
.controller('profile_settingsCtrl', function($scope, $rootScope, $location, $cookies, Restangular, $route, toastr){
	var my_articlesAPI = Restangular.all('/account/articles/get');
	var deleteDraftAPI = Restangular.all('/article/delete');
	var loadProfileAPI = Restangular.all('/account/info/load');
	
	$scope.profile = {};
	$scope.profile.photo_url = '';
	$scope.pagination = {received: 0, requested: 5, maxed: true};
	$scope.articles = [];
	//getArticles($cookies.get('access_token'));
	loadProfile($cookies.get('access_token'));
	$scope.active_tab = {};
	$scope.draft_publish = "";
	evaluateActiveTab();
	
	//confirm leaving the page
    $rootScope.confirm_location_change = false;
	
	function evaluateActiveTab(draft_publish){
		$scope.draft_publish = draft_publish;
		if($route.current.activetab == 'settings'){
			$scope.active_tab.settings = true;		
		}
		if($route.current.activetab == 'my-articles'){
			$scope.active_tab.myarticles = true;
			if(draft_publish == 'publish' || $rootScope.publish_route){
				$scope.draft_publish = 'publish';
				getArticles($cookies.get('access_token'), 0, $scope.pagination);
			}
			else{
				$scope.draft_publish = 'draft';
				getArticles($cookies.get('access_token'), 1, $scope.pagination);
			}
		}
	}
	
	$scope.loadMore = function(page, draft_publish){
		if(draft_publish == 'publish'){
			getArticles($cookies.get('access_token'), 0, $scope.pagination);
		}
		else{
			getArticles($cookies.get('access_token'), 1, $scope.pagination);
		}
	}
	
	$scope.evaluateActiveTab = function(draft_publish){
		$rootScope.publish_route = false;
		$scope.articles = [];
		$scope.pagination = {received: 0, requested: 5, maxed: true};
		evaluateActiveTab(draft_publish);
	}

	$scope.deleteDraft = function(article, access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token, article_id: article.id};
        deleteDraftAPI.post(params).then(function(response){
			$rootScope.loading = false;
			toastr.success('Your draft has been deleted','Draft');
            $route.reload();
			$rootScope.publish_route = false;
        }, function(error){
            console.log(error);
        });
	}
	
	$scope.goToMyArticles = function(){
		$location.path('/my-articles');
	}
	$scope.goToSettings = function(){
		$location.path('/settings');
	}
	
	function loadProfile(access_token){
		$rootScope.loading = true;
		var params = {access_token: access_token};
        loadProfileAPI.post(params).then(function(response){
			$rootScope.loading = false;
            $scope.profile = response.user;
			$scope.profile.original_username = response.user.username;
			$rootScope.profile_photo = response.user.photo_url;
        }, function(error){
			$rootScope.loading = false;
            console.log(error);
        });
	}
	function getArticles(access_token, is_draft, page){
		$rootScope.loading_background = 'transparent';
		$rootScope.loading = true;
		var params = {access_token: access_token, is_draft: is_draft, number_of_articles_received: page.received, number_of_articles_requested: page.requested};
        my_articlesAPI.post(params).then(function(response){
			$rootScope.loading = false;
			page.maxed = false;
				$rootScope.loading = false;
				for (var i=0; i<response.articles.length; i++) {
					$scope.articles.push(response.articles[i]);
				}
				page.received += response.articles.length;
				if(page.requested > response.articles.length || response.articles.length == 0){
					page.maxed = true;
				}
        }, function(error){
            console.log(error);
			$rootScope.loading = false;
        }).finally(function(){
			$rootScope.loading_background = '';
		});
	}
});