angular.module('mainApp')
.controller('create_slideshowCtrl', function($scope, $rootScope, $cookies, Restangular, baseUrl, $location, $routeParams, $q, Upload){
	var get_slideshowAPI = Restangular.all('/slideshow/load');
    
    $scope.slideshow = {};
    $scope.slideshow.photos = [];
    $scope.slideshow.timestamp = formatDateTime(new Date());
    $scope.slideshow.type = 'slideshow';
    
    //confirm leaving the page
    $rootScope.confirm_location_change = true;

    $scope.sortableOptions = { 
        containerPositioning:'relative',
        additionalPlaceholderClass: 'col-sm-4',
        //clone: true,
        //containment: '#board',
        //restrict move across columns. move only within column.
        accept: function (sourceItemHandleScope, destSortableScope) {
            //console.log(sourceItemHandleScope.itemScope.sortableScope.$id + " " + destSortableScope.$id);
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        },
        orderChanged: function (event){
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    };
    
    $scope.multiSelectCredit = function(credit){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.credit = credit;
        });
    }
    $scope.multiSelectTitle = function(title){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.title = title;
        });
    }
    $scope.multiSelectCaption = function(caption){
        angular.forEach($scope.slideshow.photos, function(photo){
            photo.alt = caption;
        });
    }
    
    function sortOrderIds(slideshow_photos){
        angular.forEach(slideshow_photos, function(photo, index){
            photo.order_id = index;
        });
        return slideshow_photos;
    }
    
    $scope.$watch('slideshow_photos', function (photos) {
        if($scope.slideshow_photos){
            $scope.slideshow.photos.push.apply($scope.slideshow.photos, photos);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    });
    
    //$scope.$watch('slideshow.photos', function (photos) {
    //    $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
    //});
    
    $scope.deleteImage = function(image){
        if(image.size){ //check if image is a file
            $scope.slideshow.photos.splice(image.order_id, 1);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
        if(image.id){ //if the image has an id its a url and not a file
            $scope.slideshow.deleted_images.push({id: image.id}); //add the deleted id to array for proper deletion at the server
            $scope.slideshow.photos.splice(image.order_id, 1);
            $scope.slideshow.photos = sortOrderIds($scope.slideshow.photos);
        }
    }
    
   
    //If id is set in the url get that slideshow
    if($routeParams.id){
        $rootScope.loading = true;
        var params = {access_token: $cookies.get('access_token'), article_id: $routeParams.id};
        get_slideshowAPI.post(params).then(function(response){
            $rootScope.loading = false;
            $scope.slideshow = response.article;
            $scope.slideshow.photos = response.images;
            $scope.slideshow.deleted_images = []; //to keep track of deleted images || photos.id
            $scope.slideshow.existing_images = []; //to keep track of the existing url's for saved images and their order || photos.id and photos.order_id
            //$rootScope.$broadcast('elastic:adjust');
            var converted_date = new Date(Date.parse($scope.slideshow.created_at));
			$scope.slideshow.timestamp = formatDateTime(converted_date);
        }, function(error){
            $rootScope.loading = false;
            console.log(error);
            $location.path('/');
        });
    }
    
    function formatDateTime(now){
        var timestamp = {};
        timestamp.time = now.getHours() + ':' + now.getMinutes() + ':' + now.getSeconds();
        timestamp.weekday = getWeekday(now.getDay());
        timestamp.day = getGetOrdinal(now.getDate());
        timestamp.month = getMonth(now.getMonth());
        timestamp.year =  now.getFullYear();
        return timestamp;
    }
    function getWeekday(day_number){
        var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        return days[day_number];
    }
    function getMonth(month_number){
        var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        return months[month_number];
    }
    function getGetOrdinal(n) {
        var s=["th","st","nd","rd"];
        var v=n%100;
        return n+(s[(v-20)%10]||s[v]||s[0]);
    }
	
	//For handling multiple uploads
    function uploads(token, slideshow_id, photos_url, files){
        var deferred = $q.defer();
        var queue = [];

        if (files && files.length) {
            for (var i = 0; i < files.length; i++) {
                var promise = uploadImage(token, slideshow_id, photos_url, files[i]);
                queue.push(promise);
            }
        }

        $q.all(queue).then(
            function (data) {
                deferred.resolve(data)
            },
            function (err) {
                deferred.reject(err)
            }
        )

        return deferred.promise
    }
    //Called from uploads-function
    function uploadImage(token, slideshow_id, url, file) {
        var promise = Upload.upload({
            url: url,
            headers: {'Content-Type': "multipart/form-data"},
            method: 'POST',
            fields: {access_token: token, slideshow_id: slideshow_id},
            file: file
        }).progress(function (evt) {
            var progressPercentage = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
            file.progress = progressPercentage;
        }).success(function (data, status, headers, config) {
            file.result = 'file ' + config.file.name + ' uploaded. Response: ' + data;
            $scope.test_picture = data.photo_url;
        });
        return promise;
    };
});